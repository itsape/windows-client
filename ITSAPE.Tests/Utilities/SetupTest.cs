﻿using System;
using System.IO;
using System.Reflection;
using ITSAPE.Client.Utilities;
using ITSAPE_Client.Models;
using Newtonsoft.Json;
using Xunit;

namespace ITSAPE.Tests.Utilities
{
    public class SetupTest
    {
        private string GetInstallDir()
        => Path.GetDirectoryName(Path.TrimEndingDirectorySeparator(AppContext.BaseDirectory));

        [Fact]
        public void GetInstallDir_ReturnsAssemblyDir()
        {
            var setup = new Setup();


            var dir = setup.GetInstallPath();


            Assert.Equal(GetInstallDir(), dir);
        }

        [Fact]
        public void GetConfiguration_LoadsConfigFile()
        {
            var setup = new Setup();
            var cfg = new Configuration
            {
                ConnectivityInterval = 1000,
                DnsSetting = new Configuration.Dns
                {
                    ItsApe = "127.0.0.1",
                    Standard = "127.0.0.2"
                },
                GatewaySetting = new Configuration.Gateway
                {
                    ItsApe = "127.0.0.3",
                    Standard = "127.0.0.4"
                },
                ProxySetting = new Configuration.UProxy
                {
                    Host = "127.0.0.5",
                    Interval = 1000,
                    Port = 1337
                },
                CertThumbprint = "asd123"
            };
            var cfgFile = Path.Combine(GetInstallDir(), "config.json");
            Assert.False(File.Exists(cfgFile));
            File.WriteAllText(cfgFile, JsonConvert.SerializeObject(cfg));
            Assert.True(File.Exists(cfgFile));


            var actualCfg = setup.GetConfiguration();


            Assert.Equal(cfg.ConnectivityInterval, actualCfg.ConnectivityInterval);
            Assert.Equal(cfg.DnsSetting.ItsApe, actualCfg.DnsSetting.ItsApe);
            Assert.Equal(cfg.DnsSetting.Standard, actualCfg.DnsSetting.Standard);
            Assert.Equal(cfg.GatewaySetting.ItsApe, actualCfg.GatewaySetting.ItsApe);
            Assert.Equal(cfg.GatewaySetting.Standard, actualCfg.GatewaySetting.Standard);
            Assert.Equal(cfg.ProxySetting.Interval, actualCfg.ProxySetting.Interval);
            Assert.Equal(cfg.ProxySetting.Host, actualCfg.ProxySetting.Host);
            Assert.Equal(cfg.ProxySetting.Port, actualCfg.ProxySetting.Port);
            Assert.Equal(cfg.CertThumbprint, actualCfg.CertThumbprint);


            File.Delete(cfgFile);
        }
    }
}
