﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITSAPE.Client.Jobs;
using ITSAPE.Client.Utilities;
using ITSAPE_Client.Jobs;
using ITSAPE_Client.Models;
using ITSAPE_Client.Service;
using ITSAPE_Client.Utilities;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;
using ISetup = ITSAPE_Client.Utilities.ISetup;

namespace ITSAPE.Tests.Jobs
{
    public class JobManagerTest
    {
        [Fact]
        public void CreateDirectory_CreatesDirectory()
        {
            var cwd = Path.GetFullPath(Path.Combine(Path.GetTempPath(),
                "itsape_test_" + new Random().Next()));
            var file = Path.Combine(cwd, "test.file");
            var userUtil = new Mock<ILoginUserUtility>();
            var logger = new Mock<ILogger<JobManager>>();
            var setup = new Mock<ISetup>();
            setup.Setup(s => s.GetInstallPath()).Returns(cwd);
            var clientFactory = new Mock<IJobPollerWebClientFactory>();
            var detectorService = new Mock<IDetectorService>();
            var os = new Mock<IOs>();
            var jm = new JobManager(userUtil.Object, logger.Object, setup.Object, clientFactory.Object,
                detectorService.Object, os.Object);

            Assert.False(Directory.Exists(cwd));

            var result = jm.CreateDirectory(file);

            Assert.True(result);
            Assert.True(Directory.Exists(cwd));

            Directory.Delete(cwd, true);
        }

        [Fact]
        public void ExecuteCopyJob_DoesNotOverwriteExisting()
        {
            var cwd = Path.GetFullPath(Path.Combine(Path.GetTempPath(),
                "itsape_test_" + new Random().Next()));
            Directory.CreateDirectory(cwd);
            var job = new Job {
                Type = "copy",
                DeployPath = Path.Combine(cwd, "test.file")
            };

            var userUtil = new Mock<ILoginUserUtility>();
            var logger = new Mock<ILogger<JobManager>>();
            var setup = new Mock<ISetup>();
            setup.Setup(s => s.GetInstallPath()).Returns(cwd);
            var clientFactory = new Mock<IJobPollerWebClientFactory>();
            var detectorService = new Mock<IDetectorService>();
            var os = new Mock<IOs>();
            var jm = new JobManager(userUtil.Object, logger.Object, setup.Object, clientFactory.Object,
                detectorService.Object, os.Object);

            Assert.False(File.Exists(job.DeployPath));
            File.Create(job.DeployPath).Close();
            Assert.True(File.Exists(job.DeployPath));

            
            var result = jm.ExecuteCopyJob(job);


            Assert.Equal(ProxyResponse.StatusCode.Failed, result.Status);

            Directory.Delete(cwd, true);
        }

        [Fact]
        public void ExecuteCopyJob_DownloadsFile()
        {
            var cwd = Path.GetFullPath(Path.Combine(Path.GetTempPath(),
                "itsape_test_" + new Random().Next()));
            Directory.CreateDirectory(cwd);
            var job = new Job
            {
                Id = "1",
                Type = "copy",
                DeployPath = Path.Combine(cwd, "test.file")
            };

            var userUtil = new Mock<ILoginUserUtility>();
            var logger = new Mock<ILogger<JobManager>>();
            var setup = new Mock<ISetup>();
            setup.Setup(s => s.GetInstallPath()).Returns(cwd);
            var client = new Mock<IJobPollerWebClient>();

            var clientFactory = new Mock<IJobPollerWebClientFactory>();
            clientFactory.Setup(cf => cf.CreateClient()).Returns(client.Object);
            var detectorService = new Mock<IDetectorService>();
            var os = new Mock<IOs>();
            var jm = new JobManager(userUtil.Object, logger.Object, setup.Object, clientFactory.Object,
                detectorService.Object, os.Object);

            Assert.False(File.Exists(job.DeployPath));


            var result = jm.ExecuteCopyJob(job);


            Assert.Equal(ProxyResponse.StatusCode.Completed, result.Status);
            Assert.Contains(job.DeployPath, jm.copyList);
            client.Verify(c => c.DownloadFile($"/file/{job.Id}", job.DeployPath), Times.Once);

            Directory.Delete(cwd, true);
        }

        [Fact]
        public void ExecuteRemoveJob_DoesNotDeleteNotCopiedFile()
        {
            var cwd = Path.GetFullPath(Path.Combine(Path.GetTempPath(),
                "itsape_test_" + new Random().Next()));
            Directory.CreateDirectory(cwd);
            var job = new Job
            {
                Id = "1",
                Type = "remove",
                DeployPath = Path.Combine(cwd, "test.file")
            };

            var userUtil = new Mock<ILoginUserUtility>();
            var logger = new Mock<ILogger<JobManager>>();
            var setup = new Mock<ISetup>();
            setup.Setup(s => s.GetInstallPath()).Returns(cwd);
            var clientFactory = new Mock<IJobPollerWebClientFactory>();
            var detectorService = new Mock<IDetectorService>();
            var os = new Mock<IOs>();
            var jm = new JobManager(userUtil.Object, logger.Object, setup.Object, clientFactory.Object,
                detectorService.Object, os.Object);

            Assert.False(File.Exists(job.DeployPath));
            File.Create(job.DeployPath).Close();
            Assert.True(File.Exists(job.DeployPath));


            var result = jm.ExecuteRemoveJob(job);


            Assert.Equal(ProxyResponse.StatusCode.Failed, result.Status);

            Directory.Delete(cwd, true);
        }

        [Fact]
        public void ExecuteRemoveJob_DoesNotTryDeleteNotExisting()
        {
            var cwd = Path.GetFullPath(Path.Combine(Path.GetTempPath(),
                "itsape_test_" + new Random().Next()));
            Directory.CreateDirectory(cwd);
            var job = new Job
            {
                Id = "1",
                Type = "remove",
                DeployPath = Path.Combine(cwd, "test.file")
            };

            var userUtil = new Mock<ILoginUserUtility>();
            var logger = new Mock<ILogger<JobManager>>();
            var setup = new Mock<ISetup>();
            setup.Setup(s => s.GetInstallPath()).Returns(cwd);
            var clientFactory = new Mock<IJobPollerWebClientFactory>();
            var detectorService = new Mock<IDetectorService>();
            var os = new Mock<IOs>();
            var jm = new JobManager(userUtil.Object, logger.Object, setup.Object, clientFactory.Object,
                detectorService.Object, os.Object);

            Assert.False(File.Exists(job.DeployPath));


            var result = jm.ExecuteRemoveJob(job);


            Assert.Equal(ProxyResponse.StatusCode.Failed, result.Status);

            Directory.Delete(cwd, true);
        }

        [Fact]
        public void ExecuteRemoveJob_DeletesFile()
        {
            var cwd = Path.GetFullPath(Path.Combine(Path.GetTempPath(),
                "itsape_test_" + new Random().Next()));
            Directory.CreateDirectory(cwd);
            var job = new Job
            {
                Id = "1",
                Type = "remove",
                DeployPath = Path.Combine(cwd, "test.file")
            };

            var userUtil = new Mock<ILoginUserUtility>();
            var logger = new Mock<ILogger<JobManager>>();
            var setup = new Mock<ISetup>();
            setup.Setup(s => s.GetInstallPath()).Returns(cwd);
            var clientFactory = new Mock<IJobPollerWebClientFactory>();
            var detectorService = new Mock<IDetectorService>();
            var os = new Mock<IOs>();
            var jm = new JobManager(userUtil.Object, logger.Object, setup.Object, clientFactory.Object,
                detectorService.Object, os.Object);

            Assert.False(File.Exists(job.DeployPath));
            File.Create(job.DeployPath).Close();
            Assert.True(File.Exists(job.DeployPath));
            jm.copyList.Add(job.DeployPath);


            var result = jm.ExecuteRemoveJob(job);


            Assert.Equal(ProxyResponse.StatusCode.Completed, result.Status);
            Assert.False(File.Exists(job.DeployPath));
            Assert.DoesNotContain(job.DeployPath, jm.copyList);

            Directory.Delete(cwd, true);
        }

        [Fact]
        public void ExecuteRunJob_CallsSystem()
        {
            var cwd = Path.GetFullPath(Path.Combine(Path.GetTempPath(),
                "itsape_test_" + new Random().Next()));
            Directory.CreateDirectory(cwd);
            var job = new Job
            {
                Id = "1",
                Type = "execute",
                FilePath = Path.Combine(cwd, "test.file")
            };

            var userUtil = new Mock<ILoginUserUtility>();
            userUtil.Setup(u => u.GetUserSessionID()).Returns(0x01);
            var logger = new Mock<ILogger<JobManager>>();
            var setup = new Mock<ISetup>();
            setup.Setup(s => s.GetInstallPath()).Returns(cwd);
            var clientFactory = new Mock<IJobPollerWebClientFactory>();
            var detectorService = new Mock<IDetectorService>();
            var os = new Mock<IOs>();
            os.Setup(o => o.GetLastError()).Returns(0);
            os.Setup(o => o.DuplicateTokenEx(It.IsAny<IntPtr>(),
                NativeMethods.GENERIC_ALL_ACCESS,
                It.IsAny<NativeMethods.SECURITY_ATTRIBUTES>(),
                (int)NativeMethods.SECURITY_IMPERSONATION_LEVEL.SecurityIdentification,
                (int)NativeMethods.TOKEN_TYPE.TokenPrimary,
                ref It.Ref<IntPtr>.IsAny)).Returns(true);
            os.Setup(o => o.CreateProcessAsUser(It.IsAny<IntPtr>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<NativeMethods.SECURITY_ATTRIBUTES>(),
                It.IsAny<NativeMethods.SECURITY_ATTRIBUTES>(),
                It.IsAny<bool>(),
                It.IsAny<int>(),
                It.IsAny<IntPtr>(),
                It.IsAny<string>(),
                ref It.Ref<NativeMethods.STARTUPINFO>.IsAny,
                ref It.Ref<NativeMethods.PROCESS_INFORMATION>.IsAny))
                .Returns(true)
                .Verifiable();

            var jm = new JobManager(userUtil.Object, logger.Object, setup.Object, clientFactory.Object,
                detectorService.Object, os.Object);

            Assert.False(File.Exists(job.FilePath));
            File.Create(job.FilePath).Close();
            Assert.True(File.Exists(job.FilePath));
            jm.copyList.Add(job.FilePath);


            var result = jm.ExecuteRunJob(job);


            Assert.Equal(ProxyResponse.StatusCode.Completed, result.Status);
            os.Verify(o => o.CreateProcessAsUser(It.IsAny<IntPtr>(),
                It.IsAny<string>(),
                job.FilePath,
                It.Ref<NativeMethods.SECURITY_ATTRIBUTES>.IsAny,
                It.Ref<NativeMethods.SECURITY_ATTRIBUTES>.IsAny,
                It.IsAny<bool>(),
                It.IsAny<int>(),
                IntPtr.Zero,
                It.IsAny<string>(),
                ref It.Ref<NativeMethods.STARTUPINFO>.IsAny,
                ref It.Ref<NativeMethods.PROCESS_INFORMATION>.IsAny), Times.Once);

            Directory.Delete(cwd, true);
        }
    }
}
