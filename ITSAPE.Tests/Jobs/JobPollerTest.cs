﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITSAPE.Client.Jobs;
using ITSAPE_Client.Jobs;
using ITSAPE_Client.Models;
using ITSAPE_Client.Utilities;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace ITSAPE.Tests.Jobs
{
    public class JobPollerTest
    {

        [Fact]
        public void ConvertToJob_ValidJson()
        {
            var logger = new Mock<ILogger<JobPoller>>();
            var userUtil = new Mock<ILoginUserUtility>();
            var jobManager = new Mock<IJobManager>();
            var clientFactory = new Mock<IJobPollerWebClientFactory>();
            var poller = new JobPoller(logger.Object, userUtil.Object, jobManager.Object,
                clientFactory.Object);

            var ajob = new Job
            {
                Id = "0",
                Type = "execute",
                DeployPath = @"C:\\test.exe"
            };
            var testJson = JsonConvert.SerializeObject(ajob);


            var job = poller.ConvertToJob(testJson);


            Assert.NotNull(job);
            Assert.Equal(ajob.Id, job.Id);
            Assert.Equal(ajob.Type, job.Type);
            Assert.Equal(ajob.DeployPath, job.DeployPath);
        }

        [Fact]
        public void GetAllJobs_DownloadsAllJobs()
        {
            var logger = new Mock<ILogger<JobPoller>>();
            var userUtil = new Mock<ILoginUserUtility>();
            var jobManager = new Mock<IJobManager>();

            var ajobs = Enumerable.Range(0, 10).Select(id => new Job
            {
                Id = id.ToString(),
                Type = "execute",
                DeployPath = @"C:\\test.exe"
            }).ToArray();

            var client = new Mock<IJobPollerWebClient>();
            client.Setup(c => c.DownloadString(It.IsAny<string>()))
                .Returns(new Func<string,string>((uri) => JsonConvert.SerializeObject(ajobs.First(e => e.Id == uri))))
                .Verifiable();
            var clientFactory = new Mock<IJobPollerWebClientFactory>();
            clientFactory.Setup(c => c.CreateClient())
                .Returns(client.Object);
            var poller = new JobPoller(logger.Object, userUtil.Object, jobManager.Object,
                clientFactory.Object);


            var jobs = poller.GetAllJobs(ajobs.Select(e => e.Id).ToList());


            Assert.NotNull(jobs);
            Assert.Equal(ajobs.Length, jobs.Count);
            Assert.All(jobs, j =>
            {
                var aj = ajobs.First(e => e.Id == j.Id);
                Assert.Equal(aj.Type, j.Type);
                Assert.Equal(aj.DeployPath, j.DeployPath);
            });
            client.Verify(c => c.DownloadString(It.IsAny<string>()), Times.Exactly(ajobs.Length));
        }

        [Fact]
        public void RequestJobs_EmptyIfNoUser()
        {
            var logger = new Mock<ILogger<JobPoller>>();
            var userUtil = new Mock<ILoginUserUtility>();
            userUtil.Setup(u => u.GetLoggedInUser())
                .Returns("davis");
            var jobManager = new Mock<IJobManager>();
            
            var client = new Mock<IJobPollerWebClient>();
            var clientFactory = new Mock<IJobPollerWebClientFactory>();
            clientFactory.Setup(c => c.CreateClient())
                .Returns(client.Object);
            var poller = new JobPoller(logger.Object, userUtil.Object, jobManager.Object,
                clientFactory.Object);


            var jobs = poller.RequestJobs();


            Assert.NotNull(jobs);
            Assert.Empty(jobs);
            
        }

        [Fact]
        public void RequestJobs_DownloadsAllFetchedJobs()
        {
            var ajobs = Enumerable.Range(0, 10).Select(id => new Job
            {
                Id = id.ToString(),
                Type = "execute",
                DeployPath = @"C:\\test.exe"
            }).ToArray();
            var ajobsUrls = ajobs.Select(e => $"job/{e.Id}").ToArray();

            var logger = new Mock<ILogger<JobPoller>>();
            var userUtil = new Mock<ILoginUserUtility>();
            userUtil.Setup(u => u.GetLoggedInUser())
                .Returns("davis");
            var jobManager = new Mock<IJobManager>();

            var client = new Mock<IJobPollerWebClient>();
            client.Setup(c => c.DownloadString(It.IsAny<string>()))
                .Returns(new Func<string, string>((uri) =>
                {
                    if(uri == "jobs/davis")
                        return JsonConvert.SerializeObject(ajobsUrls);
                    return JsonConvert.SerializeObject(ajobs.First(e => e.Id == uri.Remove(0, 4)));
                }));
            var clientFactory = new Mock<IJobPollerWebClientFactory>();
            clientFactory.Setup(c => c.CreateClient())
                .Returns(client.Object);
            var poller = new JobPoller(logger.Object, userUtil.Object, jobManager.Object,
                clientFactory.Object);


            var jobs = poller.RequestJobs();


            Assert.NotNull(jobs);
            Assert.NotEmpty(jobs);
            Assert.Equal(ajobs.Length, jobs.Count);
            Assert.All(jobs, j =>
            {
                var aj = ajobs.First(e => e.Id == j.Id);
                Assert.Equal(aj.Type, j.Type);
                Assert.Equal(aj.DeployPath, j.DeployPath);
            });
        }
        
        [Fact]
        public void GetAndExecuteJobs_ExecutesRequestedJobs()
        {
            IList<Job> jobs = null;
            var ajobs = Enumerable.Range(0, 10).Select(id => new Job
            {
                Id = id.ToString(),
                Type = "execute",
                DeployPath = @"C:\\test.exe"
            }).ToArray();
            var ajobsUrls = ajobs.Select(e => $"job/{e.Id}").ToArray();

            var logger = new Mock<ILogger<JobPoller>>();
            var userUtil = new Mock<ILoginUserUtility>();
            userUtil.Setup(u => u.GetLoggedInUser())
                .Returns("davis");
            var jobManager = new Mock<IJobManager>();
            jobManager.Setup(j => j.ExecuteJobs(It.IsAny<IList<Job>>()))
                .Callback<IList<Job>>(list => jobs = list)
                .Verifiable();

            var client = new Mock<IJobPollerWebClient>();
            client.Setup(c => c.DownloadString(It.IsAny<string>()))
                .Returns(new Func<string, string>((uri) =>
                {
                    if(uri == "jobs/davis")
                        return JsonConvert.SerializeObject(ajobsUrls);
                    return JsonConvert.SerializeObject(ajobs.First(e => e.Id == uri.Remove(0, 4)));
                }));
            var clientFactory = new Mock<IJobPollerWebClientFactory>();
            clientFactory.Setup(c => c.CreateClient())
                .Returns(client.Object);
            var poller = new JobPoller(logger.Object, userUtil.Object, jobManager.Object,
                clientFactory.Object);


            poller.GetAndExecuteJobs(null, null);

            jobManager.Verify(j => j.ExecuteJobs(It.IsAny<IList<Job>>()), Times.Once);
            Assert.NotNull(jobs);
            Assert.NotEmpty(jobs);
            Assert.Equal(ajobs.Length, jobs.Count);
            Assert.All(jobs, j =>
            {
                var aj = ajobs.First(e => e.Id == j.Id);
                Assert.Equal(aj.Type, j.Type);
                Assert.Equal(aj.DeployPath, j.DeployPath);
            });
        }
    }
}
