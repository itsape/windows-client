﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using ITSAPE.Client.Services;
using ITSAPE.Client.Utilities;
using ITSAPE_Client.Models;
using ITSAPE_Client.Networks;
using ITSAPE_Client.Utilities;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;
using ISetup = ITSAPE_Client.Utilities.ISetup;
using Timer = System.Timers.Timer;

namespace ITSAPE.Tests.Services
{
    public class NetworkStatusServiceImplTest
    {

        [Fact]
        public void StartAsync_AbortsOnMissingConfig()
        {
            var logger = new Mock<ILogger<NetworkStatusServiceImpl>>();
            var setup = new Mock<ISetup>();
            var services= new Mock<IServiceProvider>();
            var networkManager = new Mock<INetworkManager>();
            var connectivityChecker = new Mock<IConnectivityChecker>();
            setup.Setup(e => e.GetConfiguration())
                .Throws<FileNotFoundException>()
                .Verifiable();
            var os = new Mock<IOs>();

            var service = new NetworkStatusServiceImpl(logger.Object, setup.Object, os.Object, services.Object, networkManager.Object, connectivityChecker.Object);


            service.StartAsync(CancellationToken.None).Wait();


            setup.Verify(e => e.GetConfiguration(), Times.Once);
            logger.Verify(e =>
                e.Log(LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => v.ToString().Contains("Cannot load config: Client will not be executed.")),
                    It.IsAny<Exception>(),
                    It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)), Times.Once);
        }

        [Fact]
        public void StartAsync_StartsConnectivityCheckTimer()
        {
            var logger = new Mock<ILogger<NetworkStatusServiceImpl>>();
            var setup = new Mock<ISetup>();
            setup.Setup(e => e.GetConfiguration())
                .Returns(new Configuration()
                {
                    CertThumbprint = "",
                    ConnectivityInterval = 2000,
                    DnsSetting = new Configuration.Dns(),
                    GatewaySetting = new Configuration.Gateway(),
                    ProxySetting = new Configuration.UProxy()
                })
                .Verifiable();
            setup.Setup(e => e.GetInstallPath())
                .Returns(Environment.CurrentDirectory);
            var services = new Mock<IServiceProvider>();
            var networkManager = new Mock<INetworkManager>();
            var connectivityChecker = new Mock<IConnectivityChecker>();
            var os = new Mock<IOs>();

            var service = new NetworkStatusServiceImpl(logger.Object, setup.Object, os.Object, services.Object, networkManager.Object, connectivityChecker.Object);


            service.StartAsync(CancellationToken.None).Wait();

            Assert.True(service.connectivityCheckTimer.Enabled);
            setup.Verify(e => e.GetConfiguration(), Times.Once);
            logger.Verify(e =>
                e.Log(LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => v.ToString().Contains("network status service started")),
                    It.IsAny<Exception>(),
                    It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)), Times.Once);
        }

        [Fact]
        public void StopAsync_DisposesConnectivityCheckTimer()
        {
            var logger = new Mock<ILogger<NetworkStatusServiceImpl>>();
            var setup = new Mock<ISetup>();
            var timer = new Timer();
            var os = new Mock<IOs>();
            var services = new Mock<IServiceProvider>();
            var networkManager = new Mock<INetworkManager>();
            var connectivityChecker = new Mock<IConnectivityChecker>();

            var service = new NetworkStatusServiceImpl(logger.Object, setup.Object, os.Object, services.Object, networkManager.Object, connectivityChecker.Object)
            {
                connectivityCheckTimer = timer
            };

            timer.Start();
            Assert.True(timer.Enabled);            
            
            service.StopAsync(CancellationToken.None).Wait();


            Assert.False(timer.Enabled);
        }


        [Fact]
        public void StopAsync_ResetsNetworkManager()
        {
            var logger = new Mock<ILogger<NetworkStatusServiceImpl>>();
            var setup = new Mock<ISetup>();
            var networkManager = new Mock<INetworkManager>();
            var connectivityChecker = new Mock<IConnectivityChecker>();
            var services = new Mock<IServiceProvider>();
            var os = new Mock<IOs>();

            var service = new NetworkStatusServiceImpl(logger.Object, setup.Object, os.Object, services.Object, networkManager.Object, connectivityChecker.Object);


            service.StopAsync(CancellationToken.None).Wait();


            networkManager.Verify(e => e.Change("default"), Times.Once);
            networkManager.Verify(e => e.CheckInterfaces(), Times.Once);
            networkManager.Verify(e => e.DeleteDNSSettingsFile(), Times.Once);
        }

        [Fact]
        public void StopAsync_InformsUser()
        {
            var logger = new Mock<ILogger<NetworkStatusServiceImpl>>();
            var setup = new Mock<ISetup>();
            var os = new Mock<IOs>();
            var services = new Mock<IServiceProvider>();
            var networkManager = new Mock<INetworkManager>();
            var connectivityChecker = new Mock<IConnectivityChecker>();

            var service = new NetworkStatusServiceImpl(logger.Object, setup.Object, os.Object, services.Object, networkManager.Object, connectivityChecker.Object);


            service.StopAsync(CancellationToken.None).Wait();


            logger.Verify(e =>
                e.Log(LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => v.ToString().Contains("ITSAPE-Client service stopped.")),
                    It.IsAny<Exception>(),
                    It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)), Times.Once);
        }
    }
}
