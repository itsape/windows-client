#!/bin/sh

# Usage: patch-tables.sh <MSI>
# This script reads .patch.idt files of the tables specified in TABLES,
# replaces any variables and appends the result to the existing tables of <MSI>

TABLES="CustomAction InstallExecuteSequence"
SRC="$(dirname "$0")"
TMP="$SRC/tmp"
MSI="$1"

mkdir "$TMP"

for t in $TABLES; do
    msiinfo export "$MSI" "$t" > "$TMP/$t.idt"
    envsubst < "$SRC/$t.patch.idt" >> "$TMP/$t.idt"
    unix2dos "$TMP/$t.idt"
    echo "patching $t"
    msibuild "$MSI" -i "$TMP/$t.idt" || { echo 'msibuild failed' ; exit 1; }
done

rm -rf "$TMP"
