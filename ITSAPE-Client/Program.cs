﻿using System;
using ITSAPE_Client.Service;
using System.ServiceProcess;
using System.Threading.Tasks;
using ITSAPE.Client.Jobs;
using ITSAPE.Client.Utilities;
using ITSAPE_Client.Jobs;
using ITSAPE_Client.Networks;
using ITSAPE_Client.Utilities;
using ItsApe.ArtifactDetector;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ItsApe.ArtifactDetector.Services;
using ITSAPE.Client.Services;

namespace ITSAPE.Client
{
    internal static class Program
    {
        public static Task Main()
        {
            return BuildHost().RunAsync();
        }
        private static IHost BuildHost()
        {
            return new HostBuilder()
#if DEBUG
                .ConfigureServices(services =>
                {
                    services.AddSingleton<ILifetimeEventDispatcher, LifetimeEventDispatcher>();
                })
#else
                .UseApeWindowsService(o =>
                {
                    o.ServiceName = "ITS.APE Client";
                })
#endif
                .UseDefaultServiceProvider((hostContext, providerOptions) =>
                {
                    //enable strict integrity checks
                    providerOptions.ValidateOnBuild = true;
                    providerOptions.ValidateScopes = true;
                })
                .ConfigureServices(services =>
                {
                    services.AddLogging(c =>
                    {
                        c.AddEventLog();
#if DEBUG
                        c.AddConsole();
#endif
                    });

                    services.AddSingleton<IOs, WindowsOs>();
                    services.AddSingleton<ISessionManager, SessionManager>();
                    services.AddSingleton<ISystemHelper, SystemHelper>();
                    services.AddSingleton<ISetup, Setup>();
                    services.AddSingleton<ArtifactDetectorConfiguration>(s => s.GetRequiredService<ISetup>().GetConfiguration().ArtifactDetectorConfiguration);
                    services.AddSingleton<ILoginUserUtility, LoginUserUtility>();
                    services.AddSingleton<IJobPollerWebClientFactory>(s =>
                    {
                        var configuration = s.GetRequiredService<ISetup>().GetConfiguration();
                        return new JobPollerWebClientFactory(s.GetRequiredService<ILogger<JobPollerWebClientFactory>>(), configuration.CertThumbprint,
                            $"https://{configuration.ProxySetting.Host}:{configuration.ProxySetting.Port}",
                            s);
                    });
                    services.AddSingleton<IJobManager, JobManager>();
                    services.AddSingleton<IJobPoller, JobPoller>();

                    services.AddSingleton<INetworkManager>(s =>
                    {
                        var setup = s.GetRequiredService<ISetup>();
                        var configuration = setup.GetConfiguration();
                        return new NetworkManager(s.GetRequiredService<ILogger<NetworkManager>>(), setup)
                        {
                            ConnectivityInterval = configuration.ConnectivityInterval
                        };
                    });
                    services.AddSingleton<IConnectivityChecker>(s =>
                    {
                        var configuration = s.GetRequiredService<ISetup>().GetConfiguration();
                        return new ConnectivityChecker(s.GetRequiredService<INetworkManager>(),
                            s.GetRequiredService<ILogger<ConnectivityChecker>>(),
                            configuration.DnsSetting.ItsApe,
                            s.GetRequiredService<IOs>());
                    });
                    services.AddSingleton<IDetectorService, DetectorService>();
                })
                .ConfigureServices((services) =>
                {
                    services.AddHostedService<NetworkStatusServiceImpl>();
                    services.AddHostedService<JobPollerServiceImpl>();
                    services.AddHostedService<IDetectorService>(s => s.GetRequiredService<IDetectorService>());
                })
                .Build();
        }
    }
}