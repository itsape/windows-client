using System;
using System.IO;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Timers;
using ItsApe.ArtifactDetector.Converters;
using ItsApe.ArtifactDetector.DetectorConditions;
using ItsApe.ArtifactDetector.Detectors;
using ItsApe.ArtifactDetector.Models;
using ItsApe.ArtifactDetector.Services;
using ITSAPE.Client.Utilities;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.WindowsServices;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace ITSAPE.Client.Services
{
    public class ApeWindowsServiceLifetime : WindowsServiceLifetime
    {
        private readonly ILifetimeEventDispatcher _lifetimeEvent;

        public ApeWindowsServiceLifetime(IHostEnvironment environment,
            IHostApplicationLifetime applicationLifetime,
            ILoggerFactory loggerFactory,
            IOptions<HostOptions> optionsAccessor,
            IOptions<WindowsServiceLifetimeOptions> windowsServiceOptionsAccessor,
            ILifetimeEventDispatcher lifetimeEvent) : base(environment, applicationLifetime, loggerFactory, optionsAccessor, windowsServiceOptionsAccessor)
        {
            _lifetimeEvent = lifetimeEvent;
            
            CanHandleSessionChangeEvent = true;
            CanHandlePowerEvent = true;
        }

        /// <summary>
        /// Continueing when the service was paused by the OS.
        /// </summary>
        protected override void OnContinue()
        {
            _lifetimeEvent.DispatchContinue();
            base.OnContinue();
        }

        /// <summary>
        /// Pause signal from the OS.
        /// </summary>
        protected override void OnPause()
        {
            _lifetimeEvent.DispatchPause();
            base.OnPause();
        }

        /// <summary>
        /// Power event from the OS.
        /// </summary>
        /// <param name="powerStatus">Information about the new status.</param>
        protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus)
        {
            _lifetimeEvent.DispatchPowerEvent(powerStatus);
            return base.OnPowerEvent(powerStatus);
        }

        /// <summary>
        /// Method is called by system whenever a session is changed.
        /// </summary>
        /// <param name="changeDescription">Struct with further info about the change.</param>
        protected override void OnSessionChange(SessionChangeDescription changeDescription)
        {
            _lifetimeEvent.DispatchSessionChange(changeDescription);
            base.OnSessionChange(changeDescription);
        }

        /// <summary>
        /// Handle shutdown of service by system.
        /// </summary>
        protected override void OnShutdown()
        {
            _lifetimeEvent.DispatchShutdown();
            base.OnShutdown();
        }
    }
}