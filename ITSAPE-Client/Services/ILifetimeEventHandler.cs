using System.ServiceProcess;
using Microsoft.Extensions.Logging;

namespace ITSAPE.Client.Services
{
    public interface ILifetimeEventHandler
    {
        /// <summary>
        /// Continueing when the service was paused by the OS.
        /// </summary>
        void OnContinue();

        /// <summary>
        /// Pause signal from the OS.
        /// </summary>
        void OnPause();

        /// <summary>
        /// Power event from the OS.
        /// </summary>
        /// <param name="powerStatus">Information about the new status.</param>
        void OnPowerEvent(PowerBroadcastStatus powerStatus);

        /// <summary>
        /// Method is called by system whenever a session is changed.
        /// </summary>
        /// <param name="changeDescription">Struct with further info about the change.</param>
        void OnSessionChange(SessionChangeDescription changeDescription);

        /// <summary>
        /// Handle shutdown of service by system.
        /// </summary>
        void OnShutdown();

    }
}