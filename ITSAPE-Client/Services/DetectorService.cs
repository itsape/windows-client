﻿using System;
using System.Diagnostics;
using System.IO;
using System.ServiceModel;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using ItsApe.ArtifactDetector.Converters;
using ItsApe.ArtifactDetector.DetectorConditions;
using ItsApe.ArtifactDetector.Detectors;
using ItsApe.ArtifactDetector.Models;
using ITSAPE_Client.Service;
using ITSAPE_Client.Utilities;
using ITSAPE.Client.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Timer = System.Timers.Timer;
using Microsoft.Extensions.Logging.Abstractions;

namespace ItsApe.ArtifactDetector.Services
{
    /// <summary>
    /// Detector service class that waits for another process to call "StartWatch", then tries to detect
    /// the configured artifact until "StopWatch" is called.
    /// </summary>
    partial class DetectorService : IDetectorService, ILifetimeEventHandler
    {
        /// <summary>
        /// File name of the configuration file.
        /// </summary>
        private const string ConfigurationFile = "detector_state.json";

        /// <summary>
        /// A log writer for detection results.
        /// </summary>
        private DetectionLogWriter detectionLogWriter;

        /// <summary>
        /// Timer to call detection in interval.
        /// </summary>
        private Timer detectionTimer = null;

        /// <summary>
        /// Timer to do health checks for process.
        /// </summary>
        private Timer healthCheckTimer = null;
        

        /// <summary>
        /// Variables describing the current state of the service, to be serialized and saved in the configuration file.
        /// </summary>
        private DetectorServiceState _detectorServiceState;

        /// <summary>
        /// Manager instance for user sessions.
        /// </summary>
        private readonly ISessionManager _sessionManager;

        private IServiceProvider _services;

        private readonly ILifetimeEventDispatcher _lifetimeEventDispatcher;

        private readonly JsonSerializerSettings jsonSerializerSettings;

        /// <summary>
        /// Instantiate service.
        /// </summary>
        public DetectorService(ISetup setup, ILogger<DetectorService> logger, ISessionManager sessionManager, IServiceProvider services, ILifetimeEventDispatcher ILifetimeEventDispatcher)
        {
            _sessionManager = sessionManager;
            Setup = setup;
            Logger = logger;
            _services = services;
            _lifetimeEventDispatcher = ILifetimeEventDispatcher;
            jsonSerializerSettings = new JsonSerializerSettings()
            {
                Converters =
                    {
                        new ArtifactRuntimeInformationConverter(_services),
                        new DetectorConverter(_services),
                        new DetectorConditionConverter<ArtifactRuntimeInformation>()
                    },
                
            };
            Logger.LogInformation("Detector service initialized.");
        }

        /// <summary>
        /// Logger instance for this class.
        /// </summary>
        private ILogger Logger { get; }

        /// <summary>
        /// Setup for this run, holding arguments and other necessary objects.
        /// </summary>
        private ISetup Setup { get; }

        /// <summary>
        /// Start watching an artifact in an interval of configured length.
        /// </summary>
        /// <param name="jsonEncodedParameters">Optional paramaters, JSON encoded. Only optional if called in OnStart!</param>
        public bool StartWatch(string jsonEncodedParameters = "")
        {
            // Get the state as fields might not be persisted.
            EnsureServiceState();

            // Test the current service state to be sure.
            if (_detectorServiceState.IsRunning)
            {
                Logger.LogError("Can't start watch since it is already running.");
                return false;
            }

            // Set flag of this to "isRunning" early to only start one watch task at a time.
            _detectorServiceState.IsRunning = true;
            PersistServiceState();

            // Check parameters for validity.
            if ((jsonEncodedParameters == null || jsonEncodedParameters == "") && _detectorServiceState.DetectorConfiguration == null)
            {
                Logger.LogError("Invalid or empty argument for StartWatch. Not going to execute watch task.");
                _detectorServiceState.IsRunning = false;
                PersistServiceState();
                return false;
            }

            // Only have to do this if we got parameters.
            try
            {
                _detectorServiceState.DetectorConfiguration = JsonConvert.DeserializeObject<DetectorConfiguration>(jsonEncodedParameters, jsonSerializerSettings);
            }
            catch (Exception e)
            {
                Logger.LogError("Exception while deserializing JSON parameters: {0}.", e.Message);
                _detectorServiceState.IsRunning = false;
                PersistServiceState();
                return false;
            }

            Logger.LogDebug("Creating new detection log writer.");
            detectionLogWriter = new DetectionLogWriter(
                Setup.GetInstallPath(), _detectorServiceState.DetectorConfiguration.RuntimeInformation.ArtifactName, _services.GetRequiredService<ILogger<DetectionLogWriter>>());

            // Start detection loop.
            Logger.LogInformation("Starting watch task now with interval of {0}ms.", _detectorServiceState.DetectorConfiguration.DetectionInterval);
            detectionTimer = new Timer
            {
                Interval = _detectorServiceState.DetectorConfiguration.DetectionInterval,
            };
            detectionTimer.Elapsed += DetectionEventHandler;
            detectionTimer.Start();
            PersistServiceState();

            return true;
        }
        

        /// <summary>
        /// Stop watching the artifact currently watched.
        /// </summary>
        public string StopWatch(string jsonEncodedParameters)
        {
            StopWatchParameters parameters;

            try
            {
                parameters = JsonConvert.DeserializeObject<StopWatchParameters>(jsonEncodedParameters, jsonSerializerSettings);
            }
            catch (Exception)
            {
                return "";
            }

            // Get the state as fields are not persisted.
            EnsureServiceState();

            if (!_detectorServiceState.IsRunning)
            {
                return "";
            }

            // Stop detection loop, wait for finishing and collect results.
            detectionTimer.Stop();

            // Set configuration to null to be empty on next run.
            _detectorServiceState.DetectorConfiguration = null;

            // Make ready for next watch task.
            _detectorServiceState.IsRunning = false;

            PersistServiceState();

            return detectionLogWriter.CompileResponses(parameters.ErrorWindowSize);
        }

        
        
        /// <summary>
        /// Starts this service by opening a service host.
        /// </summary>
        /// <param name="args"></param>
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            // Uncomment this to debug.
            // Debugger.Launch();

            // Get service state from file.
            Logger.LogDebug("Retrieving saved service state.");
            EnsureServiceState();
            

            RestartSavedState();
            Logger.LogInformation("Detector service started.");

            _lifetimeEventDispatcher.AddHandler(this);
            
            // Check if process is running every 60 seconds.
            healthCheckTimer = new Timer(30 * 1000);
            healthCheckTimer.Elapsed += HealthCheckProcesses;
            healthCheckTimer.Start();
        }

        /// <summary>
        /// Stops service by closing the service host.
        /// </summary>
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _lifetimeEventDispatcher.RemoveHandler(this);
            // Save configuration to file.
            PersistServiceState();

            // Dispose detection timer just in case.
            if (detectionTimer != null)
            {
                detectionTimer.Dispose();
            }

            Logger.LogInformation("Detector service stopped. Bye bye!");
        }

        /// <summary>
        /// Method that gets continuously called by timer (in intervals) to detect the artifact.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="eventArgs"></param>
        private void DetectionEventHandler(object source, ElapsedEventArgs eventArgs)
        {
            // Fire detection with a new runtime information copy to work with and forget the task.
            Task.Factory.StartNew(() => TriggerDetection(
                _detectorServiceState.DetectorConfiguration.Detector,
                (ArtifactRuntimeInformation)_detectorServiceState.DetectorConfiguration.RuntimeInformation.Clone(),
                _detectorServiceState.DetectorConfiguration.MatchConditions,
                ref detectionLogWriter));

            // Uncomment for debugging purposes, if needed.
            // detectionTimer.Elapsed -= DetectionEventHandler;
        }

        /// <summary>
        /// Get state of the service, either from file or new object.
        /// </summary>
        private void EnsureServiceState()
        {
            // Do not do unnecessary work.
            if (_detectorServiceState != null)
            {
                return;
            }

            // Get file and read from it, if exists.
            string fileName = Uri.UnescapeDataString(
                Path.Combine(Setup.GetInstallPath(), ConfigurationFile));
            var configurationFileInfo = new FileInfo(fileName);
            if (configurationFileInfo.Exists)
            {
                Logger.LogInformation("Restoring configuration from file.");
                using (var reader = new StreamReader(configurationFileInfo.FullName))
                {
                    _detectorServiceState = JsonConvert.DeserializeObject<DetectorServiceState>(reader.ReadToEnd(), jsonSerializerSettings);
                }
            }
            // The config file may exist, but not hold a service state (yet)
            if (_detectorServiceState == null)
            {
                Logger.LogInformation("Creating new configuration.");
                _detectorServiceState = new DetectorServiceState();
            }
        }

        /// <summary>
        /// Ensure all processes are running, called by Timer.Elapsed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HealthCheckProcesses(object sender, ElapsedEventArgs e)
        {
            _sessionManager.HealthCheckProcesses();
        }

        /// <summary>
        /// "Pause" the service state: Either temporarily or for a shutdown etc.
        /// </summary>
        private void PauseCurrentState()
        {
            // Dispose detection timer.
            if (detectionTimer != null)
            {
                detectionTimer.Dispose();
                detectionTimer = null;
            }

            _detectorServiceState.IsRunning = false;

            // Save configuration to file.
            PersistServiceState();
            Logger.LogInformation("Detector service stopped. Bye bye!");
        }

        /// <summary>
        /// Save service state to file.
        /// </summary>
        private void PersistServiceState()
        {
            // Save configuration to file.
            string jsonEncodedConfig = JsonConvert.SerializeObject(
                _detectorServiceState,
                jsonSerializerSettings);

            string fileName = Uri.UnescapeDataString(
            Path.Combine(Setup.GetInstallPath(), ConfigurationFile));
            using (var writer = new StreamWriter(fileName))
            {
                Logger.LogInformation("Saving configuration to file");
                writer.Write(jsonEncodedConfig);
            }
        }

        private void RestartSavedState()
        {
            // First: Make sure we have the service state.
            EnsureServiceState();
            

            // Check in which state the service was paused.
            if (_detectorServiceState.IsRunning)
            {
                Logger.LogInformation("Watch task should be running, start it.");
                _detectorServiceState.IsRunning = false;
                StartWatch();
            }
        }

        /// <summary>
        /// Method to detect the currently given artifact and write the response to the responses log file.
        ///
        /// WARNING: This gets executed in a new instance!
        /// WARNING: Almost no try-catch is done in here to save resources!
        /// </summary>
        private void TriggerDetection(IDetector detector, ArtifactRuntimeInformation runtimeInformation, IDetectorCondition<ArtifactRuntimeInformation> matchConditions, ref DetectionLogWriter detectionLogWriter)
        {
            var queryTime = DateTime.UtcNow;
            if (!_sessionManager.HasActiveSessions())
            {
                // No active user sessions, no artifacts can be present.
                Logger.LogInformation("No session active, no detection necessary.");
                detectionLogWriter.LogDetectionResult(
                    queryTime, new DetectorResponse { ArtifactPresent = DetectorResponse.ArtifactPresence.Impossible });
                return;
            }

            // Loop through all active sessions and detect separately.
            DetectorResponse detectorResponse = null;
            foreach (var sessionEntry in _sessionManager.DetectorProcesses)
            {
                try
                {
                    detectorResponse = detector.FindArtifact(ref runtimeInformation, matchConditions, sessionEntry.Key);
                    detectionLogWriter.LogDetectionResult(queryTime, detectorResponse);
                }
                catch (Exception e)
                {
                    Logger.LogError("Could not execute FindArtifact: \"{0}\" \"{1}\"", e.Message, e.InnerException.Message);
                    detectionLogWriter.LogDetectionResult(
                        queryTime, new DetectorResponse { ArtifactPresent = DetectorResponse.ArtifactPresence.Possible });
                }
            }
        }

        public void OnContinue()
        {
            RestartSavedState();
            Logger.LogInformation("Ape continued");
        }

        public void OnPause()
        {
            PauseCurrentState();
            Logger.LogInformation("Ape paused");
        }

        public void OnPowerEvent(PowerBroadcastStatus powerStatus)
        {
            switch (powerStatus)
            {
                case PowerBroadcastStatus.QuerySuspend:
                    PauseCurrentState();
                    Logger.LogInformation("Received suspend signal, paused service state");
                    break;

                case PowerBroadcastStatus.QuerySuspendFailed:
                    RestartSavedState();
                    Logger.LogInformation("Received suspend signal failed, restarted saved service state");
                    break;

                case PowerBroadcastStatus.ResumeAutomatic:
                case PowerBroadcastStatus.ResumeCritical:
                case PowerBroadcastStatus.ResumeSuspend:
                    RestartSavedState();
                    Logger.LogInformation("Received resume signal, restarted saved service state");
                    break;
            }


        }

        public void OnSessionChange(SessionChangeDescription changeDescription)
        {
            switch (changeDescription.Reason)
            {
                case SessionChangeReason.ConsoleConnect:
                case SessionChangeReason.RemoteConnect:
                case SessionChangeReason.SessionLogon:
                case SessionChangeReason.SessionUnlock:
                    _sessionManager.IncreaseSessionCounter(changeDescription.SessionId);
                    break;

                case SessionChangeReason.ConsoleDisconnect:
                case SessionChangeReason.RemoteDisconnect:
                case SessionChangeReason.SessionLock:
                case SessionChangeReason.SessionLogoff:
                    _sessionManager.DecreaseSessionCounter(changeDescription.SessionId);
                    break;
            }

        }

        public void OnShutdown()
        {
            PauseCurrentState();
        }
    }
}
