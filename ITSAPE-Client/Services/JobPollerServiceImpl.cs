﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using ITSAPE.Client.Utilities;
using ITSAPE_Client.Jobs;
using ITSAPE_Client.Models;
using ITSAPE_Client.Utilities;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Timer = System.Timers.Timer;

namespace ITSAPE.Client.Services
{
    public class JobPollerServiceImpl : IHostedService
    {
        /// <summary>
        /// Instance of job poller to use for JobPollTimer.
        /// </summary>
        private IJobPoller jobPoller;

        /// <summary>
        /// Timer to periodically poll for new jobs.
        /// </summary>
        internal Timer jobPollTimer;

        private readonly ILogger _logger;
        private readonly ISetup _setup;

        public JobPollerServiceImpl(
            ILogger<JobPollerServiceImpl> logger,
            ISetup setup,
            IJobPoller jobPoller
            )
        {
            _logger = logger;
            _setup = setup;
            this.jobPoller = jobPoller;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {

            // Loading configuration or stop client if unable to load.
            Configuration configuration;
            try
            {
                configuration = _setup.GetConfiguration();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Cannot load config: Client will not be executed.");
                await StopAsync(CancellationToken.None);
                return;
            }

            // Start timer for job polling.
            jobPollTimer = new Timer
            {
                Interval = configuration.ProxySetting.Interval,
                AutoReset = true,
                Enabled = true
            };
            
            jobPollTimer.Elapsed += new ElapsedEventHandler(jobPoller.GetAndExecuteJobs);
            jobPollTimer.Start();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            // Stop thread for job polling.
            if (jobPollTimer != null)
            {
                jobPollTimer.Stop();
                jobPollTimer.Dispose();
            }

            _logger.LogInformation("ITSAPE-Client service stopped.");

            return Task.CompletedTask;
        }
        
    }
}
