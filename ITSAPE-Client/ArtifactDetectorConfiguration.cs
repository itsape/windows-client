﻿using System;
using System.Configuration;

namespace ItsApe.ArtifactDetector
{
    /// <summary>
    /// Class for configuration of the AAD.
    /// </summary>
    public class ArtifactDetectorConfiguration
    {
        /// <summary>
        /// Name of the feature extractor to use.
        /// </summary>
        public string FeatureExtractor { get; set; }

        /// <summary>
        /// Feature extraction setting: Threshold to sort out possible matches by their distance.
        /// </summary>
        public double MatchDistanceThreshold{ get; set; }

        /// <summary>
        /// Feature extraction setting: Name of the match filter to use.
        /// </summary>
        public string MatchFilter{ get; set; }
        

        /// <summary>
        /// Feature extraction setting: Uniqueness threshold to sort out possible matches.
        /// </summary>
        public double MatchUniquenessThreshold { get; set; }

        /// <summary>
        /// Feature extraction setting: Absolute count of matches needed for a match.
        /// </summary>
        public int MinimumMatchesRequired{ get; set; } 

        /// <summary>
        /// The name of the application runnign in the user session.
        /// </summary>
        public string UserSessionApplicationName{ get; set; }
    }
}
