﻿namespace ITSAPE_Client.Utilities
{
    public interface ILoginUserUtility
    {
        /// <summary>
        /// Find out the currently logged in user.
        /// </summary>
        /// <returns>Username of logged in user.</returns>
        string GetLoggedInUser();

        /// <summary>
        /// Find out the currently logged in user's session ID.
        /// </summary>
        /// <returns>Session ID of logged in user.</returns>
        uint GetUserSessionID();
    }
}