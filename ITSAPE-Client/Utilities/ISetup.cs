﻿using ITSAPE_Client.Models;
using Newtonsoft.Json;
using System.IO;
using System.Reflection;

namespace ITSAPE_Client.Utilities
{
    public interface ISetup
    {
        string ApplicationGuid { get; }

        Configuration GetConfiguration();
        string GetInstallPath();
    }
}