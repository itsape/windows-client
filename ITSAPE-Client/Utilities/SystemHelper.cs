﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITSAPE.Client.Utilities
{
    public class SystemHelper : ISystemHelper
    {
        /// <summary>
        /// Get information from the OS about the currently logged in user.
        /// </summary>
        /// <returns>Information string for the logged in user.</returns>
        public string GetLoggedInUserInformation()
        {
            // TODO: What will be returned if NO user is currently logged in?
            // Prepare process to find out the user.
            string nsExecParams = "user";
            Process process = new Process
            {
                StartInfo = new ProcessStartInfo("query", nsExecParams)
                {
                    UseShellExecute = false,
                    ErrorDialog = false,
                    RedirectStandardOutput = true
                }
            };
            string queryResult = "";
            bool processStarted = process.Start();
            using (StreamReader nsAnswer = process.StandardOutput)
            {
                queryResult = nsAnswer.ReadToEnd();
                process.WaitForExit();
            }

            return queryResult;
        }
    }
}
