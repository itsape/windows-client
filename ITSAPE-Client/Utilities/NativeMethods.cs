﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace ITSAPE.Client.Utilities
{
    /// <summary>
    /// Wrapper class for DLLImports and system calls.
    /// </summary>
    public class NativeMethods
    {
        public const int GENERIC_ALL_ACCESS = 0x10000000;

        public enum SECURITY_IMPERSONATION_LEVEL
        {
            SecurityAnonymous,
            SecurityIdentification,
            SecurityImpersonation,
            SecurityDelegation
        }

        public enum TOKEN_TYPE
        {
            TokenPrimary = 1,
            TokenImpersonation
        }
        [DllImport("kernel32", EntryPoint = "GetLastError", SetLastError = true, CharSet = CharSet.Unicode)]
        internal static extern uint GetLastError();

        [DllImport("dnsapi.dll", EntryPoint = "DnsFlushResolverCacheEntry_A")]
        public static extern int DnsFlushResolverCacheEntry(string hostName);

        [DllImport("kernel32.dll",
            EntryPoint = "CloseHandle", SetLastError = true,
            CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool CloseHandle(IntPtr handle);

        [DllImport("advapi32.dll",
            EntryPoint = "CreateProcessAsUser", SetLastError = true,
            CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern bool
            CreateProcessAsUser(IntPtr hToken, string lpApplicationName, string lpCommandLine,
                SECURITY_ATTRIBUTES lpProcessAttributes, SECURITY_ATTRIBUTES lpThreadAttributes,
                bool bInheritHandle, Int32 dwCreationFlags, IntPtr lpEnvrionment,
                string lpCurrentDirectory, ref STARTUPINFO lpStartupInfo,
                ref PROCESS_INFORMATION lpProcessInformation);

        [DllImport("advapi32.dll",
            EntryPoint = "DuplicateTokenEx",
            CharSet = CharSet.Auto,
            SetLastError = true)]
        public static extern bool
            DuplicateTokenEx(IntPtr hExistingToken, Int32 dwDesiredAccess,
                SECURITY_ATTRIBUTES lpThreadAttributes,
                Int32 ImpersonationLevel, Int32 dwTokenType,
                ref IntPtr phNewToken);

        [DllImport("kernel32.dll")]
        public static extern uint WTSGetActiveConsoleSessionId();

        [DllImport("wtsapi32.dll", SetLastError = true)]
        public static extern bool WTSQueryUserToken(UInt32 sessionId, out IntPtr Token);

        [StructLayout(LayoutKind.Sequential)]
        public struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public Int32 dwProcessID;
            public Int32 dwThreadID;
        }

        
        [StructLayout(LayoutKind.Sequential)]
        public class SECURITY_ATTRIBUTES : IDisposable
        {
            public int nLength;
            public IntPtr lpSecurityDescriptor;
            public int bInheritHandle;

            public SECURITY_ATTRIBUTES()
            {
                nLength = Marshal.SizeOf(typeof(SECURITY_ATTRIBUTES));
                lpSecurityDescriptor = IntPtr.Zero;
                bInheritHandle = 1;
            }

            public SECURITY_ATTRIBUTES(RawSecurityDescriptor sec) :
                this()
            {
                byte[] binDACL = new byte[sec.BinaryLength];
                sec.GetBinaryForm(binDACL, 0);

                lpSecurityDescriptor = Marshal.AllocHGlobal(sec.BinaryLength);

                Marshal.Copy(binDACL, 0, lpSecurityDescriptor, sec.BinaryLength);
            }

            ~SECURITY_ATTRIBUTES()
            {
                Dispose(false);
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool disposing)
            {
                if (disposing)
                {

                }

                if (lpSecurityDescriptor != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(lpSecurityDescriptor);
                    lpSecurityDescriptor = IntPtr.Zero;
                }
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct STARTUPINFO
        {
            public Int32 cb;
            public string lpReserved;
            public string lpDesktop;
            public string lpTitle;
            public Int32 dwX;
            public Int32 dwY;
            public Int32 dwXSize;
            public Int32 dwXCountChars;
            public Int32 dwYCountChars;
            public Int32 dwFillAttribute;
            public Int32 dwFlags;
            public Int16 wShowWindow;
            public Int16 cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        internal const int TokenGeneralAccess = 0x10000000;
        internal const int WtsCurrentSession = -1;

        internal enum SecurityImpersionationLevel
        {
            SecurityAnonymous,
            SecurityIdentification,
            SecurityImpersonation,
            SecurityDelegation
        }

        internal enum TokenType
        {
            TokenPrimary = 1,
            TokenImpersonation
        }
        
        [DllImport("kernel32.dll", EntryPoint = "CreateFileMapping", SetLastError = true, CharSet = CharSet.Unicode)]
        internal static extern IntPtr CreateFileMapping(UIntPtr hFile, SECURITY_ATTRIBUTES lpAttributes, uint flProtect, uint dwMaximumSizeHigh, uint dwMaximumSizeLow, string lpName);

        [DllImport("kernel32.dll", EntryPoint = "MapViewOfFile", SetLastError = true, CharSet = CharSet.Unicode)]
        internal static extern IntPtr MapViewOfFile(IntPtr hFileMappingObject, uint dwDesiredAccess, uint dwFileOffsetHigh, uint dwFileOffsetLow, uint /* UIntPtr */ dwNumberOfBytesToMap);

        [DllImport("kernel32.dll", EntryPoint = "UnmapViewOfFile", SetLastError = true, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.VariantBool)]
        internal static extern bool UnmapViewOfFile(IntPtr lpBaseAddress);
        

        [DllImport("advapi32.dll", EntryPoint = "CreateProcessAsUser", SetLastError = true,
            CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool CreateProcessAsUserW(
            IntPtr hToken, string lpApplicationName, string lpCommandLine,
            ref SecurityAttributes lpProcessAttributes, ref SecurityAttributes lpThreadAttributes,
            bool bInheritHandle, uint dwCreationFlags, IntPtr lpEnvrionment,
            string lpCurrentDirectory, ref Startupinfo lpStartupInfo,
            ref ProcessInformation lpProcessInformation);

        [DllImport("advapi32.dll", EntryPoint = "DuplicateTokenEx")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool DuplicateTokenEx(
            IntPtr hExistingToken, uint dwDesiredAccess,
            ref SecurityAttributes lpThreadAttributes,
            SecurityImpersionationLevel ImpersonationLevel, TokenType dwTokenType,
            ref IntPtr phNewToken);

        [DllImport("wtsapi32.dll", SetLastError = true)]
        internal static extern int WTSEnumerateSessions(IntPtr hServer, int Reserved, int Version,
            ref IntPtr ppSessionInfo, ref int pCount);

        [DllImport("Wtsapi32.dll")]
        internal static extern void WTSFreeMemory(IntPtr pointer);
        

        #region enums

        internal enum WtsConnectedState
        {
            WTSActive,
            WTSConnected,
            WTSConnectQuery,
            WTSShadow,
            WTSDisconnected,
            WTSIdle,
            WTSListen,
            WTSReset,
            WTSDown,
            WTSInit
        }

        internal enum WtsInfoClass
        {
            WTSInitialProgram,
            WTSApplicationName,
            WTSWorkingDirectory,
            WTSOEMId,
            WTSSessionId,
            WTSUserName,
            WTSWinStationName,
            WTSDomainName,
            WTSConnectState,
            WTSClientBuildNumber,
            WTSClientName,
            WTSClientDirectory,
            WTSClientProductId,
            WTSClientHardwareId,
            WTSClientAddress,
            WTSClientDisplay,
            WTSClientProtocolType,
            WTSIdleTime,
            WTSLogonTime,
            WTSIncomingBytes,
            WTSOutgoingBytes,
            WTSIncomingFrames,
            WTSOutgoingFrames,
            WTSClientInfo,
            WTSSessionInfo,
            WTSSessionInfoEx,
            WTSConfigInfo,
            WTSValidationInfo,
            WTSSessionAddressV4,
            WTSIsRemoteSession
        }

        #endregion enums

        #region structs

        [StructLayout(LayoutKind.Sequential)]
        internal struct ProcessInformation
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public Int32 dwProcessID;
            public Int32 dwThreadID;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct SecurityAttributes
        {
            public Int32 Length;
            public IntPtr lpSecurityDescriptor;
            public bool bInheritHandle;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Startupinfo
        {
            public int cb;
            public string lpReserved;
            public string lpDesktop;
            public string lpTitle;
            public int dwX;
            public int dwY;
            public int dwXSize;
            public int dwXCountChars;
            public int dwYCountChars;
            public int dwFillAttribute;
            public int dwFlags;
            public short wShowWindow;
            public short cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct WtsSessionInfo
        {
            public int SessionId;

            [MarshalAs(UnmanagedType.LPStr)] public string pWinStationName;

            public WtsConnectedState State;
        }

        #endregion structs

    }
}
