﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITSAPE.Client.Utilities
{
    public interface IOs
    {
        bool CloseHandle(IntPtr handle);

        bool
            CreateProcessAsUser(IntPtr hToken, string lpApplicationName, string lpCommandLine,
                NativeMethods.SECURITY_ATTRIBUTES lpProcessAttributes, NativeMethods.SECURITY_ATTRIBUTES lpThreadAttributes,
                bool bInheritHandle, Int32 dwCreationFlags, IntPtr lpEnvrionment,
                string lpCurrentDirectory, ref NativeMethods.STARTUPINFO lpStartupInfo,
                ref NativeMethods.PROCESS_INFORMATION lpProcessInformation);

        bool
            DuplicateTokenEx(IntPtr hExistingToken, Int32 dwDesiredAccess,
                NativeMethods.SECURITY_ATTRIBUTES lpThreadAttributes,
                Int32 ImpersonationLevel, Int32 dwTokenType,
                ref IntPtr phNewToken);


        uint WTSGetActiveConsoleSessionId();


        bool WTSQueryUserToken(UInt32 sessionId, out IntPtr Token);
        int GetLastError();


        int DnsFlushResolverCacheEntry(string hostName);
    }
}
