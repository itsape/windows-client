﻿using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using ITSAPE.Client.Utilities;
using Microsoft.Extensions.Logging;

[assembly: InternalsVisibleTo("ITSAPE.Tests")]
namespace ITSAPE_Client.Utilities
{
    public class LoginUserUtility : ILoginUserUtility
    {
        /// <summary>
        /// Logger to write log messages to.
        /// </summary>
        private ILogger eventLog;

        /// <summary>
        /// Flag to tell if the logged in user has to be checked.
        /// </summary>
        private bool hasToCheckUser;

        private readonly ISystemHelper _system;

        public LoginUserUtility(ILogger<LoginUserUtility> eventLog, ISystemHelper system)
        {
            this.eventLog = eventLog;
            _system = system;
        }

        /// <summary>
        /// Find out the currently logged in user.
        /// </summary>
        /// <returns>Username of logged in user.</returns>
        public string GetLoggedInUser()
        {
            string userInformation = _system.GetLoggedInUserInformation();

            // Parse username that is marked active
            Regex userRegex = new Regex(@"^[>|\s](.+)\s{2,}[^\s]+\s{2,}[^\s]+\s{2,}A[ck]tive?", RegexOptions.Multiline);
            MatchCollection userMatch = userRegex.Matches(userInformation);

            if (userMatch.Count > 0)
            {
                GroupCollection data = userMatch[userMatch.Count - 1].Groups;
                hasToCheckUser = true;
                return data[1].ToString().Trim();
            }
            else
            {
                if (hasToCheckUser == true)
                    eventLog.LogWarning("Cannot determine active user (no one logged in?): " + userInformation.ToString());
                hasToCheckUser = false;
            }

            return "";
        }

        /// <summary>
        /// Find out the currently logged in user's session ID.
        /// </summary>
        /// <returns>Session ID of logged in user.</returns>
        public uint GetUserSessionID()
        {
            string userInformation = _system.GetLoggedInUserInformation();

            // Parse sesson id that is marked active
            Regex userRegex = new Regex(@"(\d+)\s+A[ck]tiv[e]*", RegexOptions.Multiline);
            MatchCollection userMatch = userRegex.Matches(userInformation);
            if (userMatch.Count > 0)
            {
                GroupCollection data = userMatch[userMatch.Count - 1].Groups;
                return uint.Parse(data[1].Value);
            }
            else
                eventLog.LogWarning("Cannot determine active session:\n\n" + userInformation.ToString());

            return 0;
        }

        
    }
}