﻿using ITSAPE_Client.Utilities;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Timers;
using ITSAPE.Client.Utilities;

namespace ITSAPE_Client.Networks
{
    /// <summary>
    /// Checks for internet connectivity.
    /// </summary>
    public class ConnectivityChecker : IConnectivityChecker
    {
        /// <summary>
        /// Domain to use for checking connectivity.
        /// </summary>
        private readonly string checkingDomain;

        /// <summary>
        /// DNS server for ITS.APT.
        /// </summary>
        private readonly string itsAptDNSServer;

        /// <summary>
        /// Logger to write log messages to.
        /// </summary>
        private ILogger eventLog;

        /// <summary>
        /// Flag to tell whether we are connected (to the internet).
        /// </summary>
        private bool isConnected;

        /// <summary>
        /// Status of the last DNS check, true if it succeeded.
        /// </summary>
        private bool LastDNSCheckSucceeded;

        /// <summary>
        /// Status of the last internet connectivity check, true if it succeeded.
        /// </summary>
        private bool LastWWWCheckSucceeded;

        /// <summary>
        /// Network manager to handle DNS settings and more.
        /// </summary>
        private INetworkManager networkManager;

        private readonly IOs _os;

        /// <summary>
        /// Constructor for the connectivity (to the internet) checker.
        /// </summary>
        /// <param name="networkManager">Network manager to manage DNS settings and more.</param>
        /// <param name="eventLog">Logger to write log messages to.</param>
        /// <param name="itsAptDNSServer">DNS server for ITS.APT.</param>
        /// <param name="checkingDomain">Domain to use for checking connectivity (to the internet).</param>
        public ConnectivityChecker(INetworkManager networkManager, ILogger<ConnectivityChecker> eventLog, string itsAptDNSServer, IOs os, string checkingDomain = "www.google.de")
        {
            this.networkManager = networkManager;
            this.eventLog = eventLog;
            this.itsAptDNSServer = itsAptDNSServer;
            _os = os;
            this.checkingDomain = checkingDomain;

            LastDNSCheckSucceeded = true;
            LastWWWCheckSucceeded = true;
        }

        /// <summary>
        /// Flushes hostName from the DNS cache.
        /// </summary>
        /// <param name="hostName">The hostName to flush from the cache</param>
        public void FlushCache(string hostName)
        {
            _os.DnsFlushResolverCacheEntry(hostName);
        }

        /// <summary>
        /// Check the status of the connectivity (to the internet).
        /// To be called periodically by a Timer object.
        /// </summary>
        /// <param name="state">State of the timer.</param>
        /// <param name="e">Arguments for the elapsed event.</param>
        public void CheckStatus(object state, ElapsedEventArgs e)
        {
            try
            {
                var timer = (Timer) state;

                // Lets check our settings.
                networkManager.CheckInterfaces();

                // Test if Domain is reachable.
                if (IsDNSAvailable())
                {
                    if (LastWWWCheckSucceeded == false)
                    {
                        eventLog.LogInformation("Internet connection is up again.");
                        LastWWWCheckSucceeded = true;
                    }

                    // Get the availability of the ITS.APE DNS.
                    int itsapeDNSAvailability = IsItsApeDNSAvailable();

                    // Change config to ITSAPT if the current configuration is the default config
                    if (networkManager.IsCurrentNetworkManager("default"))
                    {
                        // Check if ITS.APE-DNS-server is available
                        switch (itsapeDNSAvailability)
                        {
                            case 0:
                                if (LastDNSCheckSucceeded == true)
                                    eventLog.LogWarning("ITS.APE-DNS not available or cannot resolve google.com!", EventLogEntryType.Warning);
                                break;

                            case 1:
                                eventLog.LogInformation("Switching config to: itsape");

                                LastDNSCheckSucceeded = true;
                                networkManager.Change("itsape");

                                if (timer != null)
                                {
                                    networkManager.ResetCheckTime();
                                    timer.Interval = networkManager.ConnectivityInterval;
                                }
                                break;

                            case 2:
                                if (LastDNSCheckSucceeded == false)
                                    eventLog.LogInformation("ITS.APE-DNS available again.");
                                break;
                        }
                    }
                    else // networkManager.CurrentNetworkManagerIs("itsape")
                    {
                        // Is user currently subject?
                        if (itsapeDNSAvailability == 0 || itsapeDNSAvailability == 2)
                        {
                            eventLog.LogInformation("Switching config to: default");
                            networkManager.Change("default");
                        }
                    }

                    LastDNSCheckSucceeded = itsapeDNSAvailability != 0;
                }
                else // Test site is not reachable.
                {
                    // Change config to default config if this is not already set.
                    if (networkManager.IsCurrentNetworkManager("itsape"))
                    {
                        eventLog.LogWarning("No Internet connectivity (itsape-config). Switching config to: default", EventLogEntryType.Warning);
                        networkManager.Change("default");
                    }
                    else if (LastWWWCheckSucceeded == true)
                    {
                        eventLog.LogWarning("No Internet connectivity (default-config).", EventLogEntryType.Warning);
                    }

                    networkManager.IncreaseCheckTime();
                    timer.Interval = networkManager.ConnectivityInterval;

                    LastWWWCheckSucceeded = false;
                }
            }
            catch (Exception error)
            {
                eventLog.LogError("ConnectivityChecker failed!\n\n" + error, EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Callback for GetHostAddress.
        /// Sets Connected to true if the result is completed and to false otherwise.
        /// </summary>
        /// <param name="asyncResult">The result.</param>
        private void GetHostAdressesCallback(IAsyncResult asyncResult)
        {
            isConnected = asyncResult.IsCompleted;
        }

        /// <summary>
        /// Try to resolve the domain within timeout seconds.
        /// </summary>
        /// <param name="timeout">The time to wait for the request to finish.</param>
        /// <returns>True if the resolve worked within the timeout, false otherwise or on error.</returns>
        private bool IsDNSAvailable(int timeout = 1000)
        {
            try
            {
                // Clear the DNS cache before requesting the domain.
                FlushCache(checkingDomain);
                isConnected = false;

                // Asynchronously lookup the Domain while sleeping.
                Dns.BeginGetHostAddresses(checkingDomain, GetHostAdressesCallback, null);
                System.Threading.Thread.Sleep(timeout);

                // Return the current value of Connected.
                return isConnected;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Check whether the ITS.APE DNS is available.
        /// </summary>
        /// <returns>0 if there is no connection to google.com (= the internet); 1 if its.apt is on 127.0.0.1; 2 if its.apt is on 127.0.0.2.</returns>
        private int IsItsApeDNSAvailable()
        {
            // First check connection to google.com (= the internet).
            var lookupResult = Nslookup(itsAptDNSServer, "google.com");
            if (!StringMatchesRegex(lookupResult, @"Name:\s+(google\.com)"))
            {
                return 0;
            }

            // Secondly check connection to its.apt.
            lookupResult = Nslookup(itsAptDNSServer, "its.apt");
            if (lookupResult.Contains("127.0.0.1"))
            {
                return 1;
            }
            else if (lookupResult.Contains("127.0.0.2"))
            {
                return 2;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Starts a new nslookup process and returns the result.
        /// </summary>
        /// <param name="dnsServer">DNS server to use for lookup.</param>
        /// <param name="domain">Domain to look up.</param>
        /// <returns>The result of the nslookup process.</returns>
        private string Nslookup(string dnsServer, string domain)
        {
            string lookupResult;
            string nsLookupParameters = domain + " " + dnsServer;

            // Initialize process for nslookup.
            Process process = new Process
            {
                StartInfo = new ProcessStartInfo("nslookup", nsLookupParameters)
                {
                    UseShellExecute = false,
                    ErrorDialog = false,
                    RedirectStandardOutput = true
                }
            };

            process.Start();
            using (StreamReader nsAnswer = process.StandardOutput)
            {
                lookupResult = nsAnswer.ReadToEnd();
            }
            process.WaitForExit();

            return lookupResult;
        }

        /// <summary>
        /// Check whether the given string matches the given regular expression.
        /// </summary>
        /// <param name="inputString">String to check.</param>
        /// <param name="regex">Regular expression to use for checking.</param>
        /// <returns>True if there is at least one match of the expression in the string.</returns>
        private bool StringMatchesRegex(string inputString, string regex)
        {
            Regex userRegex = new Regex(regex, RegexOptions.Multiline);
            return userRegex.Matches(inputString).Count > 0;
        }
    }
}