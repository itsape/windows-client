﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ITSAPE_Client.Models
{
    /// <summary>
    /// Data class to hold a job.
    /// </summary>
    public class Job
    {
        [JsonProperty("deploy_path")]
        public string DeployPath { get; internal set; }

        [JsonProperty("file_path")]
        public string FilePath { get; internal set; }

        [JsonProperty("id")]
        public string Id { get; internal set; }

        [JsonProperty("status")]
        public string Status { get; internal set; }

        [JsonProperty("action")]
        public string Type { get; internal set; }

        [JsonProperty("user")]
        public string User { get; internal set; }

        [JsonProperty("watch_arguments", Required = Required.AllowNull)]
        public WatchTaskArguments? WatchArguments { get; internal set; }

        public class WatchTaskArguments
        {
            [JsonProperty("reference_image_hash")]
            public string ReferenceImageHash { get; internal set; }

            [JsonProperty("artifact_name")]
            public string ArtifactName { get; internal set; }

            [JsonProperty("season_id")]
            public int SeasonID {get; internal set; }

            [JsonProperty("runtime_information")]
            public Dictionary<string, string> RuntimeInformation { get; internal set; }

            [JsonProperty("detectors")]
            public Dictionary<string, string> Detector { get; internal set; }

            [JsonProperty("detection_interval")]
            public int DetectionInterval { get; set; }

            [JsonProperty("match_conditions")]
            public string MatchConditions { get; set; }

            [JsonProperty("error_window_size")]
            public int ErrorWindowSize { get; set; }
        }
    }
}
