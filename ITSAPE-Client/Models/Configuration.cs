﻿using ItsApe.ArtifactDetector;
using Newtonsoft.Json;

namespace ITSAPE_Client.Models
{
    public class Configuration
    {
        public string CertThumbprint { get; set; }

        [JsonProperty("connectivity-interval")]
        public int ConnectivityInterval { get; set; }

        [JsonProperty("dns")]
        public Dns DnsSetting { get; set; }

        [JsonProperty("gateway")]
        public Gateway GatewaySetting { get; set; }

        [JsonProperty("uproxy")]
        public UProxy ProxySetting { get; set; }
        
        [JsonProperty("artifact-detector")]
        public ArtifactDetectorConfiguration ArtifactDetectorConfiguration { get; set; }


        public class Dns
        {
            [JsonProperty("itsape")]
            public string ItsApe { get; set; }

            [JsonProperty("default")]
            public string Standard { get; set; }
        }

        public class Gateway
        {
            [JsonProperty("itsape")]
            public string ItsApe { get; set; }

            [JsonProperty("default")]
            public string Standard { get; set; }
        }

        public class UProxy
        {
            [JsonProperty("host")]
            public string Host { get; set; }

            [JsonProperty("interval")]
            public int Interval { get; set; }

            [JsonProperty("port")]
            public int Port { get; set; }
        }
    }
}