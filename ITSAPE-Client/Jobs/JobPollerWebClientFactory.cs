﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITSAPE_Client.Jobs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace ITSAPE.Client.Jobs
{
    public class JobPollerWebClientFactory : IJobPollerWebClientFactory
    {
        /// <summary>
        /// Thumbprint of job server certificate.
        /// </summary>
        private readonly string certThumbprint;

        /// <summary>
        /// Logger to write log messages to.
        /// </summary>
        private readonly ILogger eventLog;


        /// <summary>
        /// Address of the server to poll for jobs.
        /// </summary>
        private readonly string jobServerAddress;

        private readonly IServiceProvider _services;

        public JobPollerWebClientFactory(ILogger<JobPollerWebClientFactory> eventLog, string certThumbprint, string jobServerAddress, IServiceProvider services)
        {
            this.eventLog = eventLog;
            this.certThumbprint = certThumbprint;
            this.jobServerAddress = jobServerAddress;
            _services = services;
        }

        public IJobPollerWebClient CreateClient()
        {
            try
            {
                return new JobPollerWebClient(_services.GetRequiredService<ILogger<JobPollerWebClient>>(),
                    certThumbprint)
                {
                    BaseAddress = jobServerAddress,
                    Encoding = Encoding.UTF8
                };
            }
            catch (Exception e)
            {
                eventLog.LogCritical("Cannot create JobPollerWebClient for {JobServerAddress}", jobServerAddress);
                throw;
            }
        }
    }
}
