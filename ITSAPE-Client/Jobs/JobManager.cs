﻿using ICSharpCode.SharpZipLib.BZip2;
using ICSharpCode.SharpZipLib.Tar;
using ITSAPE_Client.Models;
using ITSAPE_Client.Service;
using ITSAPE_Client.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.Text;
using System.Web;
using ITSAPE.Client.Jobs;
using ITSAPE.Client.Utilities;
using Microsoft.Extensions.Logging;

[assembly: InternalsVisibleTo("ITSAPE.Tests")]
namespace ITSAPE_Client.Jobs
{
    public class JobManager : IJobManager
    {
        /// <summary>
        /// URL at which files can be downloaded relatively to the base URL.
        /// </summary>
        private const string downloadsUrl = "/screenshots/";

        /// <summary>
        /// URL at which jobs can be accessed relatively to the base URL.
        /// </summary>
        private const string jobsUrl = "/jobs/";

        /// <summary>
        /// Folder name of the reference images of an artifact.
        /// </summary>
        private const string referenceImagePath = "reference_images";

        /// <summary>
        /// URL at which files can be uploaded relatively to the base URL.
        /// </summary>
        private const string uploadsUrl = "/timetables/";

        /// <summary>
        /// Full path of backup file in installation folder.
        /// </summary>
        private readonly string copyListBackupFile;

        /// <summary>
        /// Client for talking to the detecting service.
        /// </summary>
        private readonly IDetectorService detectorService;

        /// <summary>
        /// Map of artifact name to unique ID (e.g. hash) of downloaded reference images.
        /// </summary>
        private readonly Dictionary<string, string> downloadedReferenceImages = new Dictionary<string, string>();

        /// <summary>
        /// Logger to write log messages to.
        /// </summary>
        private readonly ILogger eventLog;
        

        /// <summary>
        /// Map of job types to methods that will be executed for them.
        /// </summary>
        private readonly Dictionary<string, Func<JobManager, Job, ProxyResponse>> jobTypeMap = new Dictionary<string, Func<JobManager, Job, ProxyResponse>>
        {
            {"copy", (JobManager manager, Job job) => manager.ExecuteCopyJob(job) },
            {"execute", (JobManager manager, Job job) => manager.ExecuteRunJob(job) },
            {"remove", (JobManager manager, Job job) => manager.ExecuteRemoveJob(job) },
            {"start_detection", (JobManager manager, Job job) => manager.ExecuteWatchStartJob(job) },
            {"stop_detection", (JobManager manager, Job job) => manager.ExecuteWatchStopJob(job) }
        };

        /// <summary>
        /// Controller to get information about the logged in user.
        /// </summary>
        private readonly ILoginUserUtility loginUserUtility;

        /// <summary>
        /// List of filePaths to copy.
        /// </summary>
        internal List<string> copyList;

        /// <summary>
        /// Client to send and receive HTTP messages.
        /// </summary>
        private readonly IJobPollerWebClientFactory _clientFactory ;

        private readonly ISetup _setup;

        private readonly IOs _os;

        /// <summary>
        /// </summary>
        /// <param name="loginUserUtility"></param>
        /// <param name="eventLog"></param>
        public JobManager(ILoginUserUtility loginUserUtility, ILogger<JobManager> eventLog, ISetup setup, IJobPollerWebClientFactory clientFactoryFactory, IDetectorService detectorServiceClient, IOs os)
        {
            _setup = setup;
            this.loginUserUtility = loginUserUtility;
            this.eventLog = eventLog;
            _clientFactory = clientFactoryFactory;

            copyListBackupFile = Path.Combine(setup.GetInstallPath(), "copyList.json");
            copyList = new List<string>();

            // Restore copyList from file if possible.
            RestoreCopyList();

            detectorService = detectorServiceClient;
            _os = os;
        }

        /// <summary>
        /// Execute the given job.
        /// </summary>
        /// <param name="job">The job to execute.</param>
        public void ExecuteJob(Job job)
        {
            ProxyResponse response;

            eventLog.LogInformation("Got a new job with type: " + job.Type);

            // Look job type up in map and execute the respective function.
            if (jobTypeMap.ContainsKey(job.Type))
            {
                response = jobTypeMap[job.Type](this, job);
            }
            else
            {
                // We do NOT know this job type.
                eventLog.LogWarning("Got unknown job type: " + job.Type);
                response = new ProxyResponse("Unknown job type: " + job.Type, ProxyResponse.StatusCode.Failed);
            }

            // Send result to job server.
            var username = loginUserUtility.GetLoggedInUser();
            string confirmationUri = jobsUrl + HttpUtility.UrlEncode(username) + "/" + HttpUtility.UrlEncode(job.Id);
            using var client = _clientFactory.CreateClient();
            try
            {
                client.UploadString(confirmationUri, JsonConvert.SerializeObject(response));
                eventLog.LogInformation("Sent confirmation about finished job to proxy.");
            }
            catch (WebException e1)
            {
                eventLog.LogWarning("Cannot send confirmation about finished job to proxy. Try again.\n" + e1.Message);
                try
                {
                    client.UploadString(confirmationUri, JsonConvert.SerializeObject(response));
                    eventLog.LogInformation("Sent confirmation about finished job to proxy.");
                }
                catch (WebException e2)
                {
                    eventLog.LogError("Cannot send confirmation about finished job to proxy. Give up.\n" + e2.Message);
                }
            }
        }

        /// <summary>
        /// Executes multiple jobs sequentially.
        /// </summary>
        /// <param name="jobs">The list of jobs to execute.</param>
        public void ExecuteJobs(IList<Job> jobs)
        {
            foreach (Job job in jobs)
            {
                ExecuteJob(job);
            }
        }

        /// <summary>
        /// Add file to list of previously copied files.
        /// Also persists them in JSON file.
        /// </summary>
        /// <param name="filePath">File path of copied file.</param>
        private void AddToCopyList(string filePath)
        {
            try
            {
                copyList.Add(filePath);
            }
            catch (Exception e)
            {
                eventLog.LogError("Cannot add file path to copied artifacts list:\n" + e.Message + "\n" + e.StackTrace);
            }

            // Save to file for reboot reasons.
            var jsonList = JsonConvert.SerializeObject(copyList);
            try
            {
                File.WriteAllText(copyListBackupFile, jsonList);
            }
            catch (Exception e)
            {
                eventLog.LogWarning("Cannot store copied artifacts lists. This can lead into errors while deleting files.\n" + e.Message);
            }
        }

        /// <summary>
        /// Create directory at given path (e.g. for a job).
        /// </summary>
        /// <param name="path">Absolute or relative path to create directory in.</param>
        /// <returns>True on success, false otherwise.</returns>
        internal bool CreateDirectory(string path)
        {
            try
            {
                // Get full directory object for deployPath.
                var fullDirectoryPath = Path.GetDirectoryName(path);
                Directory.CreateDirectory(fullDirectoryPath);
            }
            catch (PathTooLongException e)
            {
                eventLog.LogError("Cannot create directory to download job file (path too long). path: " + path + "\n" + e.Message);
                return false;
            }
            catch (Exception e)
            {
                eventLog.LogError("Cannot create directory to download job file. path: " + path + "\n\n" + e);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Create and run a process with user privileges.
        /// </summary>
        /// <param name="cmd">Command to run.</param>
        private void CreateProcessAsUser(string cmd)
        {
            if (cmd == null)
                return;

            IntPtr hToken = IntPtr.Zero;
            IntPtr hDupedToken = IntPtr.Zero;

            // Get session_id of currently logged in user.
            uint sessionId = loginUserUtility.GetUserSessionID();
            if (sessionId == 0xFFFFFFFF)
            {
                eventLog.LogError("No session attached to the physical console. Cannot execute: " + cmd);
                return;
            }
            else
            {
                eventLog.LogDebug("Session ID is: " + sessionId);
            }

            // Get security token for that session.
            _os.WTSQueryUserToken(sessionId, out hToken);
            int error = Marshal.GetLastWin32Error();

            // If there is an error: Log error and return.
            if (error > 0)
            {
                switch (error)
                {
                    case 1314:
                        eventLog.LogError("The caller does not have the SE_TCB_NAME privilege.");
                        break;

                    case 87:
                        eventLog.LogError("Incorrect parameter in WTSQueryUserToken()");
                        break;

                    case 2:
                        eventLog.LogError("The token query is for a session that does not exist.");
                        break;

                    case 1008:
                        eventLog.LogError("The token query is for a session in which no user is logged-on. This occurs, for example, when the session is in the idle state or SessionId is zero.");
                        break;
                }
                eventLog.LogError("Cannot execute: " + cmd);

                return;
            }

            NativeMethods.PROCESS_INFORMATION processInformation = new NativeMethods.PROCESS_INFORMATION();
            try
            {
                NativeMethods.SECURITY_ATTRIBUTES securityAttributes = new NativeMethods.SECURITY_ATTRIBUTES();

                bool result = _os.DuplicateTokenEx(
                      hToken,
                      NativeMethods.GENERIC_ALL_ACCESS,
                      securityAttributes,
                      (int)NativeMethods.SECURITY_IMPERSONATION_LEVEL.SecurityIdentification,
                      (int)NativeMethods.TOKEN_TYPE.TokenPrimary,
                      ref hDupedToken
                   );

                if (!result)
                {
                    eventLog.LogError("DuplicateTokenEx failed.");
                    throw new ApplicationException("DuplicateTokenEx failed.");
                }

                NativeMethods.STARTUPINFO startupInformation = new NativeMethods.STARTUPINFO
                {
                    lpDesktop = @"WinSta0\Default"
                };
                startupInformation.cb = Marshal.SizeOf(startupInformation);

                result = _os.CreateProcessAsUser(
                    hDupedToken,
                    null,
                    cmd,
                    null, null,
                    false, 0, IntPtr.Zero,
                    null, ref startupInformation, ref processInformation
                );

                if (!result)
                {
                    error = Marshal.GetLastWin32Error();
                    string message = string.Format("Could not CreateProcessAsUser: {0}", error);
                    eventLog.LogError(message);
                    throw new ApplicationException(message);
                }
            }
            finally
            {
                if (processInformation.hProcess != IntPtr.Zero)
                    _os.CloseHandle(processInformation.hProcess);
                if (processInformation.hThread != IntPtr.Zero)
                    _os.CloseHandle(processInformation.hThread);
                if (hDupedToken != IntPtr.Zero)
                    _os.CloseHandle(hDupedToken);
            }

        }

        /// <summary>
        /// Downloads a .tar.bz2 of reference images and extracts it to the applicaitons installation path.
        /// </summary>
        /// <param name="season_id">ID of the season for which to download the images.</param>
        /// <param name="downloadIdentifier">Unique identifier (hash) of the download.</param>
        /// <param name="referenceImagePath">Path to the reference images.</param>
        /// <returns>True on success.</returns>
        private bool DownloadReferenceImages(int season_id, string downloadIdentifier, out string targetFolder)
        {
            string tarFileName = $"{downloadIdentifier}.tar";
            string bz2FileName = $"{tarFileName}.bz2";
            string downloadAddress = $"{downloadsUrl}{season_id}";

            string targetParentFolder = Path.Combine(_setup.GetInstallPath(), $"{season_id}");
            targetFolder = Path.Combine(targetParentFolder, referenceImagePath);
            string downloadTargetFile = Path.Combine(targetParentFolder, bz2FileName);

            if (!CreateDirectory(targetParentFolder) || !CreateDirectory(targetFolder))
            {
                eventLog.LogError($"Cannot download reference images for season {season_id}\nCould not create folder.\n\n");
                return false;
            }

            using var client = _clientFactory.CreateClient();

            try
            {
                client.DownloadFile(downloadAddress, downloadTargetFile);

                using (StreamReader reader = new StreamReader(downloadTargetFile))
                using (MemoryStream tarStream = new MemoryStream())
                {
                    BZip2.Decompress(reader.BaseStream, tarStream, false);
                    TarArchive tarArchive = TarArchive.CreateInputTarArchive(tarStream);
                    tarArchive.ExtractContents(targetFolder);
                }

                return true;
            }
            catch (WebException)
            {
                eventLog.LogError($"Cannot download reference images for season {season_id}\nCould not download file.\n\n");
            }

            return false;
        }

        /// <summary>
        /// Execute job with type "copy" and return an appropriate response.
        /// </summary>
        /// <param name="job">The job of type "copy" to execute.</param>
        /// <returns>A fully initialized response object.</returns>
        internal ProxyResponse ExecuteCopyJob(Job job)
        {
            // Make sure the job directory exists.
            if (!CreateDirectory(job.DeployPath))
            {
                eventLog.LogError("Cannot download job file (ID: " + job.Id + ") to: " + job.DeployPath + "\n" + "Cannot create directory.");
                return new ProxyResponse("not copied", ProxyResponse.StatusCode.Failed);
            }

            // Don't overwrite any existing files.
            if (File.Exists(job.DeployPath))
            {
                eventLog.LogWarning("Cannot download job file (ID: " + job.Id + ") to: " + job.DeployPath + "\n" + "File already exists.");
                return new ProxyResponse("not copied - file exists", ProxyResponse.StatusCode.Failed);
            }

            using var client = _clientFactory.CreateClient();

            try
            {
                // Download the file for this job and place the file in the correct folder if necessary.
                string download_uri = "/file/" + job.Id;
                client.DownloadFile(download_uri, job.DeployPath);
            }
            catch (WebException e)
            {
                eventLog.LogError("Cannot download job file (ID: " + job.Id + ") to: " + job.DeployPath + "\n" + e.Message);
                return new ProxyResponse("not copied", ProxyResponse.StatusCode.Failed);
            }
            try
            {
                AddToCopyList(job.DeployPath); // Remember the file (to check whether we are allowed to delete it later.)
            }
            catch (Exception e)
            {
                eventLog.LogError("Cannot store file information to CopyList-file:\n" + e.Message + "\n" + e.StackTrace);
            }

            // Successful operation if here.
            eventLog.LogInformation("Copied a file to: " + job.DeployPath);

            return new ProxyResponse("copied", ProxyResponse.StatusCode.Completed);
        }

        /// <summary>
        /// Execute job with type "delete" and return an appropriate response.
        /// </summary>
        /// <param name="job">The job of type "delete" to execute.</param>
        /// <param name="webClient">Web client instance to download files with.</param>
        /// <returns>A fully initialized response object.</returns>
        internal ProxyResponse ExecuteRemoveJob(Job job)
        {
            // Check whether we previously copied the file to the path.
            if (!copyList.Contains(job.DeployPath))
            {
                string responseMessage = "File cannot be deleted: I can only delete files that I have copied to the client before.";
                eventLog.LogWarning(responseMessage + "\n" + JsonConvert.SerializeObject(job));
                return new ProxyResponse(responseMessage, ProxyResponse.StatusCode.Failed);
            }

            copyList.Remove(job.DeployPath);

            // Check whether file exists.
            if (!File.Exists(job.DeployPath))
            {
                eventLog.LogWarning("Could not delete a file (not found).\n" + JsonConvert.SerializeObject(job) + "\nThis may be desirable behavior.");
                return new ProxyResponse("File cannot be deleted: not found", ProxyResponse.StatusCode.Failed);
            }

            FileInfo file;
            try
            {
                file = new FileInfo(job.DeployPath);
                file.Delete();
                eventLog.LogInformation("Removed a file: " + job.DeployPath);
                return new ProxyResponse("deleted", ProxyResponse.StatusCode.Completed);
            }
            catch (IOException e)
            {
                eventLog.LogError("Could not delete a file.\n" + JsonConvert.SerializeObject(job) + "\n" + e.Message);
                return new ProxyResponse("File cannot be found: not deleted", ProxyResponse.StatusCode.Failed);
            }
            catch (UnauthorizedAccessException e)
            {
                eventLog.LogError("Could not delete a file.\n" + JsonConvert.SerializeObject(job) + "\n" + e.Message);
                return new ProxyResponse("No authorization to delete this file: not deleted", ProxyResponse.StatusCode.Failed);
            }
            catch (System.Security.SecurityException e)
            {
                eventLog.LogError("Could not delete a file." + JsonConvert.SerializeObject(job) + "\n" + e.Message);
                return new ProxyResponse("File cannot be deleted due to security reasons", ProxyResponse.StatusCode.Failed);
            }
        }

        /// <summary>
        /// Run job with type "execute" and return an appropriate response.
        /// </summary>
        /// <param name="job">The job of type "execute" to execute.</param>
        /// <param name="webClient">Web client instance to download files with.</param>
        /// <returns>A fully initialized response object.</returns>
        internal ProxyResponse ExecuteRunJob(Job job)
        {
            try
            {
                CreateProcessAsUser(job.FilePath);
            }
            catch
            {
                eventLog.LogWarning("Cannot execute file " + job.FilePath);
                return new ProxyResponse("not executed", ProxyResponse.StatusCode.Failed);
            }

            // Successful operation if here.
            eventLog.LogInformation("Executed a file: " + job.FilePath);
            return new ProxyResponse("executed", ProxyResponse.StatusCode.Completed);
        }

        /// <summary>
        /// Run job with type "watch-start".
        /// This means the detector service is called to start its watch task after downloading refernce images if necessary.
        /// </summary>
        /// <param name="job">Data for the job.</param>
        /// <returns>An appropriate response to the job server.</returns>
        private ProxyResponse ExecuteWatchStartJob(Job job)
        {
            // Download reference images if necessary.
            if (!downloadedReferenceImages.ContainsKey(job.WatchArguments.ArtifactName)
                || downloadedReferenceImages[job.WatchArguments.ArtifactName] != job.WatchArguments.ReferenceImageHash)
            {
                // Reference images for this artifact are new.

                if (downloadedReferenceImages.ContainsKey(job.WatchArguments.ArtifactName))
                {
                    // Remove old reference images for this artifact.
                    RemoveFolderContents(Path.Combine(_setup.GetInstallPath(), job.WatchArguments.ArtifactName, referenceImagePath));
                    downloadedReferenceImages.Remove(job.WatchArguments.ArtifactName);
                }

                // Download new reference images.
                if (DownloadReferenceImages(job.WatchArguments.SeasonID, job.WatchArguments.ReferenceImageHash, out string targetFolder))
                {
                    // On success: Add hash to dictionary.
                    downloadedReferenceImages.Add(job.WatchArguments.ArtifactName, job.WatchArguments.ReferenceImageHash);
                    job.WatchArguments.RuntimeInformation["reference_image_path"] = targetFolder;
                }
                else
                {
                    eventLog.LogError("Reference images could not be downloaded.");
                    return new ProxyResponse("Cannot start watch task: Reference images could not be downloaded.", ProxyResponse.StatusCode.Failed);
                }
            }

            // Start the detector service.
            if (detectorService == null)
            {
                eventLog.LogError("Detector service could not be found.");
                return new ProxyResponse("Cannot start watch task: Detector service could not be found.", ProxyResponse.StatusCode.Failed);
            }

            if (detectorService.StartWatch(JsonConvert.SerializeObject(job.WatchArguments)))
            {
                eventLog.LogInformation("Started watch task.");
                return new ProxyResponse("Started watch task.", ProxyResponse.StatusCode.Completed);
            }
            else
            {
                eventLog.LogError("Error in detector service.");
                return new ProxyResponse("Cannot start watch task: Error in detector service.", ProxyResponse.StatusCode.Failed);
            }
        }

        private ProxyResponse ExecuteWatchStopJob(Job job)
        {
            // Test if the detector service is reachable.
            if (detectorService == null)
            {
                eventLog.LogError("Detector service could not be found.");
                return new ProxyResponse("Cannot stop watch task: Detector service could not be found.", ProxyResponse.StatusCode.Failed);
            }

            // Stop the watch task and get the path to the timetable file in return.
            string timetablePath = detectorService.StopWatch(JsonConvert.SerializeObject(job.WatchArguments));

            if (timetablePath == null || timetablePath == "")
            {
                eventLog.LogError("No timetable file returned.");
                return new ProxyResponse("Error while stopping watch task: No timetable file returned.", ProxyResponse.StatusCode.Failed);
            }

            try
            {
                // Bzip2 the timetable file before uploading.
                using (FileStream reader = new FileInfo(timetablePath).OpenRead())
                using (FileStream writer = new FileInfo(timetablePath + ".bz2").Create())
                {
                    try
                    {
                        BZip2.Compress(reader, writer, true, 4096);
                    }
                    catch (BZip2Exception e)
                    {
                        eventLog.LogError("Could not zip the timetable.\n" + e.Message);
                        return new ProxyResponse("Error while stopping watch task: Could not zip the timetable.", ProxyResponse.StatusCode.Failed);
                    }
                }

                using (var client = _clientFactory.CreateClient())
                {
                    // Upload the zipped file.
                    client.UploadFile(uploadsUrl + job.User, timetablePath + ".bz2");
                }
            }
            catch (WebException e)
            {
                eventLog.LogError("Could not upload the timetable.\n" + e.Message);
                return new ProxyResponse("Error while stopping watch task: Could not upload timetable.", ProxyResponse.StatusCode.Failed);
            }

            eventLog.LogInformation("Stopped watch task.");
            return new ProxyResponse("Stopped watch task.", ProxyResponse.StatusCode.Completed);
        }

        /// <summary>
        /// Remove all files in a folder.
        /// </summary>
        /// <param name="folderPath">Absolute or relative path of the folder.</param>
        /// <returns>True on success or if the directory doesn't exist.</returns>
        private bool RemoveFolderContents(string folderPath)
        {
            try
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(folderPath);
                foreach (FileInfo file in directoryInfo.GetFiles())
                {
                    file.Delete();
                }
            }
            catch (DirectoryNotFoundException)
            {
                return true;
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get list of previously copied files from JSON backup file.
        /// </summary>
        private void RestoreCopyList()
        {
            try
            {
                using (StreamReader streamReader = new StreamReader(copyListBackupFile))
                {
                    string json = streamReader.ReadToEnd();
                    var copyList = JsonConvert.DeserializeObject<List<string>>(json);
                    if (copyList != null)
                    {
                        this.copyList = copyList;
                        eventLog.LogInformation("copied artifacts lists: " + JsonConvert.SerializeObject(copyList));
                    }
                }
            }
            catch (Exception e)
            {
                eventLog.LogWarning("Cannot load restore copied artifacts lists. This can lead into errors while deleting files.\n" + e.Message);
            }
        }

        
    }
}
