﻿using System.Collections.Generic;
using ITSAPE_Client.Models;

namespace ITSAPE_Client.Jobs
{
    public interface IJobManager
    {
        /// <summary>
        /// Execute the given job.
        /// </summary>
        /// <param name="job">The job to execute.</param>
        void ExecuteJob(Job job);

        /// <summary>
        /// Executes multiple jobs sequentially.
        /// </summary>
        /// <param name="jobs">The list of jobs to execute.</param>
        void ExecuteJobs(IList<Job> jobs);
    }
}