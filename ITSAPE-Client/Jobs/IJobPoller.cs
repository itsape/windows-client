﻿using System.Collections.Generic;
using System.Timers;
using ITSAPE_Client.Models;

namespace ITSAPE_Client.Jobs
{
    public interface IJobPoller
    {
        /// <summary>
        /// Timer callback to query jobs from the job server, download and execute them if necessary.
        /// </summary>
        /// <param name="sender">Timer object.</param>
        /// <param name="e">Arguments for this event.</param>
        void GetAndExecuteJobs(object sender, ElapsedEventArgs e);

        /// <summary>
        /// Deserialize a JSON request string to a Job object.
        /// </summary>
        /// <param name="request">JSON request.</param>
        /// <returns>The new Job object instance.</returns>
        Job ConvertToJob(string request);

        /// <summary>
        /// Retrieve a list of all jobs given by their job URIs.
        /// </summary>
        /// <param name="jobURIs">URIs of jobs to retrieve.</param>
        /// <returns>List of jobs.</returns>
        IList<Job> GetAllJobs(List<string> jobURIs);

        /// <summary>
        /// Get all jobs for currently logged in user.
        /// </summary>
        /// <returns>List of jobs, empty on error.</returns>
        IList<Job> RequestJobs();
    }
}