#!/bin/bash

# gen CA
mkdir -p {conf,private,public}
cp openssl.cnf conf/openssl.cnf
openssl req -nodes -sha256 -config conf/openssl.cnf -days 1825 -x509 -newkey rsa:4096 -out public/ca.pem -outform PEM -subj "/C=DE/ST=NRW/CN=$CACERTNAME"
mkdir signed-keys
echo "01" > conf/serial
touch conf/index
echo "unique_subject = no" > conf/index.attr

openssl x509 -in public/ca.pem -outform der -out public/ca.cer
