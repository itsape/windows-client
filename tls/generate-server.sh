# gen itsape certs
openssl genrsa -out private/server.itsape.key 4096
openssl req -new -sha256 -key private/server.itsape.key -out server.itsape.csr -subj "/C=DE/ST=NRW/CN=$HOSTNAME"\
 -addext "subjectAltName = DNS:$HOSTNAME" \
 -addext "certificatePolicies = 1.2.3.4"
openssl ca -batch -config conf/openssl.cnf -in server.itsape.csr -out public/server.itsape.pem
rm server.itsape.csr

# convert to cer
cd public
openssl x509 -in server.itsape.pem -outform der -out server.itsape.cer
