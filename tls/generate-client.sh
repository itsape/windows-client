#!/bin/sh
 
openssl genrsa -out private/client.itsape.key 4096
openssl req -new -sha256 -key private/client.itsape.key -out client.itsape.csr -subj "/C=DE/ST=NRW/CN=$CLIENTCERTNAME"\
 -addext "subjectAltName = DNS:$HOSTNAME" \
 -addext "certificatePolicies = 1.2.3.4"
openssl ca -batch -config conf/openssl.cnf -in client.itsape.csr -out public/client.itsape.pem
rm client.itsape.csr


openssl pkcs12 -inkey private/client.itsape.key -in public/client.itsape.pem -export -out private/client.itsape.pfx -passout "pass:$CLIENTPFXPASSWORD"
openssl x509 -in signed-keys/02.pem -outform der -out private/client.itsape.cer
