using System.Collections.Generic;
using ItsApe.ArtifactDetector.Models;
using ItsApe.ArtifactDetectorProcess.Detectors;
using Xunit;


namespace ArtifactDetectorProcess.Tests.Detectors
{
    public class BaseDetectorTests
    {
        [Fact]
        public void CalculateWindowVisibility_NoWindowsAbove()
        {
            var windowRect = new Rectangle(0, 0, 1000, 1000);
            var windowsAbove = new List<Rectangle>
            {
                
            };
            
            var detector = new BaseDetector();


            var percent = detector.CalculateWindowVisibility(windowRect, windowsAbove);
            
            
            Assert.Equal(100, percent);
        }
        
        [Fact]
        public void CalculateWindowVisibility_WindowHasNoAreaAndNoWindowAbove()
        {
            var windowRect = new Rectangle(0, 0, 0, 0);
            var windowsAbove = new List<Rectangle>
            {
            };
            
            var detector = new BaseDetector();


            var percent = detector.CalculateWindowVisibility(windowRect, windowsAbove);
            
            
            Assert.Equal(0, percent);
        }
        
        [Fact]
        public void CalculateWindowVisibility_WindowHasNoAreaAndWindowAbove()
        {
            var windowRect = new Rectangle(0, 0, 0, 0);
            var windowsAbove = new List<Rectangle>
            {
                new (0, 0, 100, 100)
            };
            
            var detector = new BaseDetector();


            var percent = detector.CalculateWindowVisibility(windowRect, windowsAbove);
            
            
            Assert.Equal(0, percent);
        }
        
        [Fact]
        public void CalculateWindowVisibility_PercentageCorrect()
        {
            var windowRect = new Rectangle(0, 0, 100, 100);
            var windowsAbove = new List<Rectangle>
            {
                new (0, 0, 50, 50)
            };
            
            var detector = new BaseDetector();


            var percent = detector.CalculateWindowVisibility(windowRect, windowsAbove);
            
            
            Assert.Equal(75, percent);
        }
    }
}