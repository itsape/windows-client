using System.Drawing;
using ItsApe.ArtifactDetectorProcess.Utilities;
using Moq;
using Xunit;

namespace ArtifactDetectorProcess.Tests.Utilities
{
    public class VisualCapturerTests
    {
        [Fact]
        public void GetScreenSize_SetsDpiAware()
        {
            var os = new Mock<IOs>();
            os.Setup(o => o.SetProcessDpiAwareness(It.IsAny<int>())).Verifiable();
            os.Setup(o => o.GetSystemMetrics(It.IsAny<int>())).Returns<int>((smIndex) => 200);
            var cap = new VisualCapturer(os.Object);


            var rectangle = cap.GetScreenSize();
            
            
            Assert.Equal(new Rectangle(0, 0, 200, 200), rectangle);
            os.Verify(o => o.SetProcessDpiAwareness(1), Times.Once);
        }
        
        [Fact]
        public void GetScreenSize_ReturnsCorrectRect()
        {
            var os = new Mock<IOs>();
            os.Setup(o => o.SetProcessDpiAwareness(It.IsAny<int>()));
            os.Setup(o => o.GetSystemMetrics(It.IsAny<int>())).Returns<int>((smIndex) => smIndex == 0 ? 100 : 200);
            var cap = new VisualCapturer(os.Object);


            var rectangle = cap.GetScreenSize();
            
            
            Assert.Equal(new Rectangle(0, 0, 100, 200), rectangle);
        }
    }
}