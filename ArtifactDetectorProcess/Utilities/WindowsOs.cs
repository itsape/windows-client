﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItsApe.ArtifactDetector.Utilities;

namespace ItsApe.ArtifactDetectorProcess.Utilities
{
    public class WindowsOs : IOs
    {
        public bool CloseHandle(IntPtr handle)
        {
            return NativeMethods.CloseHandle(handle);
        }

        public bool EnumWindows(NativeMethods.EnumWindowsProc enumFunc, IntPtr lParam)
        {
            return NativeMethods.EnumWindows(enumFunc, lParam);
        }

        public IntPtr FindWindow(string lpszClass, string lpszWindow)
        {
            return NativeMethods.FindWindow(lpszClass, lpszWindow);
        }

        public IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow)
        {
            return NativeMethods.FindWindowEx(hwndParent, hwndChildAfter, lpszClass, lpszWindow);
        }

        public bool GetWindowInfo(IntPtr hwnd, ref NativeMethods.WindowVisualInformation pwi)
        {
            return NativeMethods.GetWindowInfo(hwnd, ref pwi);
        }

        public bool GetWindowRect(IntPtr hWnd, ref NativeStructures.RectangularOutline rect)
        {
            return NativeMethods.GetWindowRect(hWnd, ref rect);
        }

        public int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount)
        {
            return NativeMethods.GetWindowText(hWnd, lpString, nMaxCount);
        }

        public int GetWindowTextLength(IntPtr hWnd)
        {
            return NativeMethods.GetWindowTextLength(hWnd);
        }

        public uint GetWindowThreadProcessId(IntPtr hWnd, out uint dwProcessId)
        {
            return NativeMethods.GetWindowThreadProcessId(hWnd, out dwProcessId);
        }

        public bool IsIconic(IntPtr hWnd)
        {
            return NativeMethods.IsIconic(hWnd);
        }

        public bool IsWindowVisible(IntPtr hWnd)
        {
            return NativeMethods.IsWindowVisible(hWnd);
        }

        public IntPtr OpenProcess(uint dwDesiredAccess, bool bInheritHandle, uint dwProcessId)
        {
            return NativeMethods.OpenProcess(dwDesiredAccess, bInheritHandle, dwProcessId);
        }

        public bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, UIntPtr nSize,
            ref uint vNumberOfBytesRead)
        {
            return NativeMethods.ReadProcessMemory(hProcess, lpBaseAddress, lpBuffer, nSize, ref vNumberOfBytesRead);
        }

        public IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam)
        {
            return NativeMethods.SendMessage(hWnd, Msg, wParam, lParam);
        }

        public IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, UIntPtr dwSize, uint flAllocationType, uint flProtect)
        {
            return NativeMethods.VirtualAllocEx(hProcess, lpAddress, dwSize, flAllocationType, flProtect);
        }

        public bool VirtualFreeEx(IntPtr hProcess, IntPtr lpAddress, UIntPtr dwSize, uint dwFreeType)
        {
            return NativeMethods.VirtualFreeEx(hProcess, lpAddress, dwSize, dwFreeType);
        }

        public bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, UIntPtr nSize,
            ref uint vNumberOfBytesRead)
        {
            return NativeMethods.WriteProcessMemory(hProcess, lpBaseAddress, lpBuffer, nSize, ref vNumberOfBytesRead);
        }

        public int GetSystemMetrics(int smIndex)
        {
            return NativeMethods.GetSystemMetrics(smIndex);
        }

        public int SetProcessDpiAwareness(int value)
        {
            return NativeMethods.SetProcessDpiAwareness(value);
        }
    }
}
