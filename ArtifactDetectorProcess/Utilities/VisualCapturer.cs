﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.CompilerServices;


[assembly: InternalsVisibleTo("ArtifactDetectorProcess.Tests")]
namespace ItsApe.ArtifactDetectorProcess.Utilities
{
    /// <summary>
    /// Screenshot utility capturing windows or sub-windows.
    /// </summary>
    internal class VisualCapturer
    {
        /// <summary>
        /// Lock for screenshot utility.
        /// </summary>
        private static readonly object memoryLock = new object();

        private readonly IOs _os;

        public VisualCapturer(IOs os)
        {
            _os = os;
        }

        /// <summary>
        /// Take screenshots of all existing screens and write to memory.
        /// </summary>
        /// <param name="targetStream">Stream to write to</param>
        public void TakeScreenshots(Stream targetStream)
        {
            lock (memoryLock)
            {
                var rect = GetScreenSize();
                using var screenCaptureBitmap = new Bitmap(rect.Width, rect.Height, PixelFormat.Format32bppArgb);
                using var captureWrapper = Graphics.FromImage(screenCaptureBitmap);
                
                targetStream.Position = 0;
                captureWrapper.CopyFromScreen(rect.Left, rect.Top, 0, 0, rect.Size);
                screenCaptureBitmap.Save(targetStream, ImageFormat.Bmp);
                targetStream.Flush();
            }
        }


        internal Rectangle GetScreenSize()
        {
            _os.SetProcessDpiAwareness(1); //set application as PROCESS_SYSTEM_DPI_AWARE
            var width = _os.GetSystemMetrics(0);
            var height = _os.GetSystemMetrics(1);
            return new Rectangle(0, 0, width, height);
        }
    }
}
